//
// Created by hiiro on 3/31/20.
//
# include "edge_rotation_utils.hh"
# include "local_operations.hh"

void LocalOperations::edge_rotation(std::shared_ptr<Vector3> a, std::shared_ptr<Vector3> b) {
    if (b->is_deleted || a->is_deleted)
        return;
    //Getting the two faces related to the considered edge
    std::pair<std::shared_ptr<Face>, std::shared_ptr<Face>> p = get_faces_from_edge(a, b);
    auto f1 = p.first;
    auto f2 = p.second;

    if (!f1 || !f2)
    {
        // already moved
        return;
    }

    //Creating the hexagon around the considered edge, two of the .insert calls are inserting already inserted vertex
    std::unordered_set<std::shared_ptr<Vector3>> hexagon;
    for (auto v : f1->points)
        hexagon.insert(v);
    for (auto v : f2->points)
        hexagon.insert(v);
    size_t energy_ref = compute_energy(hexagon);

    //Testing first rotation
    auto neighbour_a_f1 = get_neighbour_on_face(b, f1);
    auto neighbour_b_f2 = get_neighbour_on_face(a, f2);
    a->del_face(f1);
    b->del_face(f2);
    neighbour_a_f1->add_face(f2);
    neighbour_b_f2->add_face(f1);
    size_t energy_new = compute_energy(hexagon);
    //If the energy is lowered, we win, so we modify the face for real
    if (energy_new < energy_ref) {
        for (int i = 0; i < 4; ++i)
        {
            if (a == f1->points[i])
                f1->points[i] = neighbour_b_f2;
            if (b == f2->points[i])
                f2->points[i] = neighbour_a_f1;
        }
        return;
    }

    //Else we gotta revert
    a->add_face(f1);
    b->add_face(f2);
    neighbour_a_f1->del_face(f2);
    neighbour_b_f2->del_face(f1);

    //Testing second rotation
    auto neighbour_a_f2 = get_neighbour_on_face(b, f2);
    auto neighbour_b_f1 = get_neighbour_on_face(a, f1);
    a->del_face(f2);
    b->del_face(f1);
    neighbour_a_f2->add_face(f1);
    neighbour_b_f1->add_face(f2);
    energy_new = compute_energy(hexagon);
    //Again, if we lowered the energy, we keep the rotation
    if (energy_new < energy_ref) {
        for (int i = 0; i < 4; ++i)
        {
            if (b == f1->points[i])
                f1->points[i] = neighbour_a_f2;
            if (a == f2->points[i])
                f2->points[i] = neighbour_b_f1;
        }
        return;
    }

    //Else we have to revert and no rotation is possible
    a->add_face(f2);
    b->add_face(f1);
    neighbour_a_f2->del_face(f1);
    neighbour_b_f1->del_face(f2);
}
