# include <algorithm>
# include <fstream>
# include <string>
# include <unordered_map>

# include "3Dobject.hh"
# include "strings.hh"

void Object3D::init(std::string filename)
{
    std::ifstream input(filename);
    if (!input.is_open())
        throw std::invalid_argument(filename);

    std::string line;
    while (getline(input,line))
    {
        auto tokens = my_split(line, ' ');
        if (tokens.size() == 0)
            continue;

        if (tokens[0] == "v")
        {
            // vertice
            this->vertices.push_back(std::make_shared<Vector3>(tokens[1], tokens[2], tokens[3]));


            // init mesh0
            this->mesh0.vertices.push_back(std::make_shared<Vector3>(tokens[1], tokens[2], tokens[3]));
        }
        else if (tokens[0] == "vt")
        {
            // texture vertice
            this->vertices_texture.push_back(std::make_shared<Vector3>(tokens[1], tokens[2], tokens[3]));
        }
        else if (tokens[0] == "vn")
        {
            // normal vertice
            this->vertices_normale.push_back(std::make_shared<Vector3>(tokens[1], tokens[2], tokens[3]));
        }
        else if (tokens[0] == "f")
        {
            // face: v/vt/vn (si pas de vt: v, si pas de vn: v)
            std::shared_ptr<Face> f = this->create_face(tokens);
            this->faces.push_back(f);
        }
    }

    input.close();

    this->link_vertices_to_face();
}

Object3D::Object3D(Object3D &other)
{
    for (auto v : other.vertices)
        this->vertices.push_back(v);
    for (auto v : other.vertices_texture)
        this->vertices_texture.push_back(v);
    for (auto v : other.vertices_normale)
        this->vertices_normale.push_back(v);
    for (auto f : other.faces)
        this->faces.push_back(f);
}

std::shared_ptr<Face> Object3D::create_face(std::vector<std::string> tokens)
{
    auto ids_point1 = my_split(tokens[1], '/');
    auto ids_point2 = my_split(tokens[2], '/');
    auto ids_point3 = my_split(tokens[3], '/');
    auto ids_point4 = my_split(tokens[4], '/');

    int id1_v = std::stoi(ids_point1[0], nullptr);
    //int id1_vt = std::stoi(ids_point1[1], nullptr);
    //int id1_vn = std::stoi(ids_point1[2], nullptr);

    int id2_v = std::stoi(ids_point2[0], nullptr);
    //int id2_vt = std::stoi(ids_point2[1], nullptr);
    //int id2_vn = std::stoi(ids_point2[2], nullptr);

    int id3_v = std::stoi(ids_point3[0], nullptr);
    //int id3_vt = std::stoi(ids_point3[1], nullptr);
    //int id3_vn = std::stoi(ids_point3[2], nullptr);

    int id4_v = std::stoi(ids_point4[0], nullptr);
    //int id4_vt = std::stoi(ids_point4[1], nullptr);
    //int id4_vn = std::stoi(ids_point4[2], nullptr);

    auto face = std::make_shared<Face>(this->vertices[id1_v - 1],
                                       this->vertices[id2_v - 1],
                                       this->vertices[id3_v - 1],
                                       this->vertices[id4_v - 1]);

    // add link vertex -> face
    this->vertex_to_link.push_back(std::make_pair(id1_v, face));
    this->vertex_to_link.push_back(std::make_pair(id2_v, face));
    this->vertex_to_link.push_back(std::make_pair(id3_v, face));
    this->vertex_to_link.push_back(std::make_pair(id4_v, face));

    // init mesh0
    std::shared_ptr<Vector3> a = std::make_shared<Vector3>(*this->vertices[id1_v - 1]);
    std::shared_ptr<Vector3> b = std::make_shared<Vector3>(*this->vertices[id2_v - 1]);
    std::shared_ptr<Vector3> c = std::make_shared<Vector3>(*this->vertices[id3_v - 1]);
    std::shared_ptr<Vector3> d = std::make_shared<Vector3>(*this->vertices[id4_v - 1]);
    std::shared_ptr<Face> f = std::make_shared<Face>(this->mesh0.vertices[id1_v - 1],
                                                     this->mesh0.vertices[id2_v - 1],
                                                     this->mesh0.vertices[id3_v - 1],
                                                     this->mesh0.vertices[id4_v - 1]);
    this->mesh0.vertices[id1_v - 1]->add_face(f);
    this->mesh0.vertices[id2_v - 1]->add_face(f);
    this->mesh0.vertices[id3_v - 1]->add_face(f);
    this->mesh0.vertices[id4_v - 1]->add_face(f);

    f->add_link_mesh0(face); // mesh0 -> mesh i
    face->add_link_mesh0(f); // mesh i -> mesh0
    this->mesh0.faces.push_back(f);

    return face;
}

void Object3D::link_vertices_to_face()
{
    for (auto p : this->vertex_to_link)
        this->vertices[p.first - 1]->add_face(p.second);

    this->vertex_to_link.clear();
}

bool Object3D::operator==(const Object3D &obj) const
{
    // TODO: un objet peut etre le meme en ayant des faces avec les points dans un ordre différent

    if (this->vertices.size() != obj.vertices.size())
        return false;

    // c'est nul, on peut opti
    for (auto v: obj.vertices)
        if (this->vertices.end() == std::find(this->vertices.begin(), this->vertices.end(), v))
            return false;
    /*
    if (this->vertices_texture.size() != obj.vertices_texture.size())
        return false;
    if (this->vertices_normale.size() != obj.vertices_normale.size())
        return false;
    */

    if (this->faces.size() != obj.faces.size())
        return false;

    // c'est nul, on peut opti
    for (auto f: obj.faces)
        if (this->faces.end() == std::find(this->faces.begin(), this->faces.end(), f))
            return false;

    return true;
}

void Object3D::to_obj(std::string filename) const
{
    std::ofstream out(filename);
    if (!out.is_open())
        throw std::invalid_argument(filename);

    std::unordered_map<std::shared_ptr<Vector3>, int> indexes;

    int i = 0;
    for (auto v : this->vertices)
    {
        out << "v ";
        v->to_obj(out);
        out << std::endl;

        indexes[v] = ++i;
    }

    out << std::endl;

    /*
    for (auto v : this->vertices_texture)
    {
        out << "vt ";
        v.to_obj(out);
        out << std::endl;
    }

    out << std::endl;

    for (auto v : this->vertices_normale)
    {
        out << "vn ";
        v.to_obj(out);
        out << std::endl;
    }

    out << std::endl;
    */

    for (auto f : this->faces)
    {
        std::string s = "f  ";
        bool ok = true;

        for (auto v: f->points)
        {
            if (v->is_deleted)
            {
                std::cout << "wtf vertex deleted: " << v->when << ": " << *v << std::endl;
                ok = false;
                break;
            }
            s += std::to_string(indexes[v]) + "/" + std::to_string(indexes[v]) + "/" + std::to_string(indexes[v]) + " ";
        }

        if (ok)
            out << s << std::endl;
    }

    out.close();
}

void Object3D::remove_face(std::shared_ptr<Face> face)
{
    auto it = this->faces.begin();
    while (it != this->faces.end() && *it != face)
        ++it;
    if (it == this->faces.end())
        throw std::invalid_argument("cannot remove face: not in obj");

    if (face->points[0]->is_related(face) || face->points[1]->is_related(face)
        || face->points[2]->is_related(face) || face->points[3]->is_related(face))
        throw std::invalid_argument("cannot remove face: still linked");

    this->faces.erase(it);
}

void Object3D::remove_vertex(std::shared_ptr<Vector3> vertex)
{
    auto it = this->vertices.begin();
    while (it != this->vertices.end() && *it != vertex)
        ++it;
    if (it == this->vertices.end())
        throw std::invalid_argument("cannot remove vertex: not in obj");

    for (auto face : vertex->related_faces)
        if (face->is_vertex(vertex))
            throw std::invalid_argument("cannot remove vertex: still linked");

    (*it)->is_deleted = true;
    this->vertices.erase(it);
}

void Object3D::add_all_to_heap()
{
    for (auto f : this->faces)
        this->diag_heap.push(f);
}

void Object3D::refresh_heap()
{
    this->diag_heap = std::priority_queue<std::shared_ptr<Face>,
                                          std::vector<std::shared_ptr<Face>>,
                                          CompareDiagonal>();
    for (auto f : this->faces)
        this->diag_heap.push(f);
}

Vector3 Object3D::get_barycenter() const
{
    Vector3 b(0, 0, 0);

    for (auto v: this->vertices)
        b += *v;

    return b / this->vertices.size();
}

std::pair<Vector3, Vector3> Object3D::bounding_box() const
{
    float min_x = this->vertices[0]->x;
    float max_x = this->vertices[0]->x;
    float min_y = this->vertices[0]->y;
    float max_y = this->vertices[0]->y;
    float min_z = this->vertices[0]->z;
    float max_z = this->vertices[0]->z;

    for (auto v : this->vertices)
    {
        if (v->x < min_x)
            min_x = v->x;
        else if (v->x > max_x)
            max_x = v->x;
        if (v->y < min_y)
            min_y = v->y;
        else if (v->y > max_y)
            max_y = v->y;
        if (v->z < min_z)
            min_z = v->z;
        else if (v->z > max_z)
            max_z = v->z;
    }

    return std::make_pair(Vector3(min_x, min_y, min_z),
                          Vector3(max_x, max_y, max_z));
}


std::ostream& operator<<(std::ostream &out, const Object3D &obj)
{
    out << "3D Object:" << std::endl
        << "Vertices:" << std::endl;
    for (auto v : obj.vertices)
        out << *v << std::endl;

    out << "Texture Vertices:" << std::endl;
    for (auto v : obj.vertices_texture)
        out << *v << std::endl;

    out << "Normal Vertices:" << std::endl;
    for (auto v : obj.vertices_normale)
        out << *v << std::endl;

    out << "Faces:" << std::endl;
    for (auto f : obj.faces)
        out << *f << std::endl;

    return out;
}
