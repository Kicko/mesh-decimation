# pragma once

# include "face.hh"

struct CompareDiagonal
{
    bool operator()(const std::shared_ptr<Face> &f1, const std::shared_ptr<Face> &f2);
};
