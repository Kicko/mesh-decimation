# include "compare.hh"
# include "vector_utils.hh"

bool CompareDiagonal::operator()(const std::shared_ptr<Face> &f1, const std::shared_ptr<Face> &f2)
{
    auto d1 = f1->get_min_diag();
    auto d2 = f2->get_min_diag();
    return distance(*d1.first, *d1.second) > distance(*d2.first, *d2.second);
}
