# pragma once

# include "vector3.hh"

class Ray
{
public:
    Ray(const Vector3 &origin, const Vector3 &unit)
        : origin(origin)
        , unit(unit)
    {}


    // attributes
    Vector3 origin;
    Vector3 unit;
};
