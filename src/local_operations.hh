# pragma once

# include "vector3.hh"
# include "face.hh"
# include "3Dobject.hh"

class LocalOperations
{
public:
    static std::shared_ptr<Vector3> diagonal_collapse(Object3D &obj);
    static void doublet_removal(Object3D &obj, std::shared_ptr<Vector3> v);
    static void edge_rotation(std::shared_ptr<Vector3> a, std::shared_ptr<Vector3> b);
    static void singlet_removal(Object3D &obj, std::shared_ptr<Vector3> v);
};
