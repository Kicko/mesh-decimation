add_library(lib_src
    vector3.cc
    face.cc
    3Dobject.cc
    compare.cc
    diagonal_collapse.cc
    removals.cc
    edge_rotation.cc
    smooth.cc
)

set(UTILS ${CMAKE_CURRENT_SOURCE_DIR}/utils)
include_directories(${UTILS})

add_subdirectory(${UTILS})

target_link_libraries(lib_src lib_utils gcov --coverage)
