# include "local_operations.hh"

Vector3 midpoint(const Vector3 &a, const Vector3 &b)
{
    return (a + b) / 2;
}

std::shared_ptr<Vector3> LocalOperations::diagonal_collapse(Object3D &obj)
{
    // heap of possible collapse, prioritized: shortest diagonal: d
    obj.refresh_heap();
    std::pair<std::shared_ptr<Vector3>,
              std::shared_ptr<Vector3>> d = obj.diag_heap.top()->get_min_diag();
    obj.diag_heap.pop();

    // diagonal d: a -> b
    std::shared_ptr<Vector3> a = d.first;
    std::shared_ptr<Vector3> b = d.second;
    if (b->is_deleted || a->is_deleted)
    {
        auto v = a;
        if (b->is_deleted)
            v = b;

        std::shared_ptr<Face> face = *v->related_faces.begin();
        Face::cut_links_vertices(face);
        obj.remove_face(face);
        return nullptr;
    }
    std::shared_ptr<Face> face = d.first->get_common_face(*b);

    auto v = std::make_shared<Vector3>(midpoint(*a, *b));

    // connect v with all faces around
    for (auto f : a->related_faces)
        if (f != face)
            v->add_face(f);
    for (auto f : b->related_faces)
        if (f != face)
            v->add_face(f);

    // change vertices of faces around
    for (auto f : a->related_faces)
        if (f != face)
            f->swap(a, v);
    for (auto f : b->related_faces)
        if (f != face)
            f->swap(b, v);

    // delete face, a and b
    Face::cut_links_vertices(face);
    obj.remove_face(face);
    obj.remove_vertex(a);
    a->when = "dia";
    obj.remove_vertex(b);
    b->when = "dia";

    // add v to obj
    obj.vertices.push_back(v);

    return v;
}
