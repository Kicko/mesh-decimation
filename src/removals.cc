# include "local_operations.hh"

void LocalOperations::doublet_removal(Object3D &obj,
                                      std::shared_ptr<Vector3> v)
{
    if (v->is_deleted || v->get_valence() != 2)
        return;

    auto it = v->related_faces.begin();
    std::shared_ptr<Face> f1 = *it;
    std::shared_ptr<Face> f2 = *(++it);

    std::shared_ptr<Vector3> v2 = nullptr;
    for (int i = 0; i < 4; ++i)
        if (!f2->points[i]->is_related(f1))
            v2 = f2->points[i];
    if (!v2)
    {
        std::cerr << "doublet_removal: two faces have same vertices" << std::endl;
        // 2 sames faces
        Face::cut_links_vertices(f1);
        Face::cut_links_vertices(f2);
        for (auto v: f1->points)
            if (v->get_valence() == 0)
            {
                obj.remove_vertex(v);
                v->when = "drem chelou";
            }
        obj.remove_face(f2);
        obj.remove_face(f1);
        return;
    }

    if (v == v2)
        throw new std::invalid_argument("v = v2, wtf");

    f1->swap(v, v2);
    Face::cut_links_vertices(f2);
    v2->add_face(f1);
    v->del_face(f1);
    obj.remove_face(f2);
    obj.remove_vertex(v);
    v->when = "drem";

    for (int i = 0; i < 4; ++i)
    {
        if (f1->points[i]->get_valence() == 2)
            LocalOperations::doublet_removal(obj, f1->points[i]);
        if (f1->points[i]->get_valence() == 1)
            LocalOperations::singlet_removal(obj, f1->points[i]);
    }
}

void LocalOperations::singlet_removal(Object3D &obj, std::shared_ptr<Vector3> v)
{
    if (v->is_deleted || v->get_valence() != 1)
        return;

    std::shared_ptr<Face> f = *v->related_faces.begin();

    std::shared_ptr<Vector3> v1 = nullptr;
    std::shared_ptr<Vector3> v2 = nullptr;
    std::shared_ptr<Vector3> v3 = nullptr;

    for (int i = 0; i < 4; ++i)
        if (f->points[i] == v)
        {
            v1 = f->points[(i + 1) % 4];
            v2 = f->points[(i + 2) % 4];
            v3 = f->points[(i + 3) % 4];
        }

    /*
            v2
           /  \
          /    \
         |      |
         |   v  |
         |  / \ |
         |  | | |
          \ | |/
           v1 v3        // in the same position
    */

    Face::cut_links_vertices(f);
    obj.remove_face(f);
    obj.remove_vertex(v);
    v->when = "srem";

    if (v3 == v1)
        return;

    for (auto face: v3->related_faces)
    {
        face->swap(v3, v1);
        v1->add_face(face);
    }

    obj.remove_vertex(v3);
    v3->when = "srem";
}
