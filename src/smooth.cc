# include "ray.hh"
# include "smooth.hh"
# include "smooth_utils.hh"
# include "vector_utils.hh"

std::shared_ptr<Vector3> tangent_smooth(std::shared_ptr<Vector3> v) {
    // barycenter of all points from 1-ring
    Vector3 barycenter = barycenter_1_ring(v);

    auto faces_v = v->related_faces;

    // normal
    Vector3 n(0, 0, 0);
    for (auto f: faces_v) {
        n += normal(*f->points[0], *f->points[1], *f->points[2]);
        n += normal(*f->points[2], *f->points[3], *f->points[0]);
    }
    n.normalize();

    // creer un rayon ray, avec barycenter comme origine et -n comme direction
    Ray ray(barycenter, -n);
    auto new_v = cast_ray(ray, *v);
    if (new_v != nullptr)
        return new_v;

    Ray ray2(barycenter, n);
    new_v = cast_ray(ray2, *v);
    return (new_v != nullptr) ? new_v : v;
}