# pragma once

# include <queue>
# include <string>
# include <utility>
# include <vector>

# include "compare.hh"
# include "face.hh"
# include "vector3.hh"

struct Mesh0
{
    std::vector<std::shared_ptr<Vector3>> vertices;
    std::vector<std::shared_ptr<Face>> faces;
};

class Object3D
{
public:
    Object3D()
    {}

    Object3D(Object3D &other);

    /* compare 2 Object3D // TODO: optimize */
    bool operator==(const Object3D &obj) const;


    /* obj file to Object3D */
    void init(std::string filename);

    /* create face from tokens (of obj file)
       token = {index vectice}/{index texture vertice}/{index normal vertice} */
    std::shared_ptr<Face> create_face(std::vector<std::string> tokens);

    /* Preprocess of getting obj
       add link vertex to face */
    void link_vertices_to_face();

    /* Object3D to obj file */
    void to_obj(std::string filename) const;

    /* Remove a face */
    void remove_face(std::shared_ptr<Face> face);

    /* Remove a vertex */
    void remove_vertex(std::shared_ptr<Vector3> vertex);

    /* Add all faces to the heap */
    void add_all_to_heap();

    /* Add all faces to the heap */
    void refresh_heap();

    /* COmpute the barycenter */
    Vector3 get_barycenter() const;

    /* Get bounding box */
    std::pair<Vector3, Vector3> bounding_box() const;


    friend std::ostream& operator<<(std::ostream &out, const Object3D &obj);


    // attributes
    std::vector<std::shared_ptr<Vector3>> vertices;
    std::vector<std::shared_ptr<Vector3>> vertices_texture;
    std::vector<std::shared_ptr<Vector3>> vertices_normale;
    std::vector<std::shared_ptr<Face>> faces;

    std::vector<std::pair<int, std::shared_ptr<Face>>> vertex_to_link;
    std::priority_queue<std::shared_ptr<Face>,
                        std::vector<std::shared_ptr<Face>>,
                        CompareDiagonal> diag_heap;

    Mesh0 mesh0;
};
