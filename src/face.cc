# include "face.hh"
# include "vector3.hh"
# include "vector_utils.hh"

bool Face::operator==(const Face &face) const
{
    for (int i = 0; i < 4; ++i)
        if (this->points[i] != face.points[i])
            return false;
    return true;
}

void Face::swap(std::shared_ptr<Vector3> oldP, std::shared_ptr<Vector3> newP)
{
    for (int i = 0; i < 4; ++i)
        if (this->points[i] == oldP)
        {
            this->points[i] = newP;
            return;
        }
    throw std::invalid_argument("Swap vertex of face");
}

bool Face::is_vertex(std::shared_ptr<Vector3> point) const
{
    for (int i = 0; i < 4; ++i)
        if (this->points[i] == point)
            return true;
    return false;
}

std::pair<std::shared_ptr<Vector3>, std::shared_ptr<Vector3>> Face::get_min_diag() const
{
    float d1 = distance(*this->points[0], *this->points[2]);
    float d2 = distance(*this->points[1], *this->points[3]);
    if (d1 > d2)
        return std::make_pair(this->points[1], this->points[3]);
    return std::make_pair(this->points[0], this->points[2]);
}

void Face::cut_links_vertices(std::shared_ptr<Face> face)
{
    for (int i = 0; i < 4; ++i)
        face->points[i]->related_faces.erase(face);
}

void Face::add_link_mesh0(std::shared_ptr<Face> face)
{
    this->link_face = face;
}

std::ostream& operator<<(std::ostream &out, const Face &face)
{
    return out << "Face:" << " "
               << *face.points[0] << " "
               << *face.points[1] << " "
               << *face.points[2] << " "
               << *face.points[3];
}
