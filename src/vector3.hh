# pragma once

# include <iostream>
# include <memory>
# include <string>
# include <unordered_set>

# include "face.hh"

class Vector3
{
public:
    Vector3(float x, float y, float z);

    /* Obj file to Vector3 */
    Vector3(std::string x, std::string y, std::string z);

    bool operator==(const Vector3 &v) const;

    Vector3 operator+(const Vector3 &v) const;
    Vector3 operator-(const Vector3 &v) const;
    Vector3 operator-() const;
    Vector3 operator*(const Vector3 &v) const;
    Vector3 operator/(const Vector3 &v) const;
    Vector3 operator*(const float &a) const;
    Vector3 operator/(const float &a) const;

    float dot(const Vector3 &v) const;
    Vector3 cross(const Vector3 &v) const;

    void operator+=(const Vector3 &v);
    void operator-=(const Vector3 &v);
    void operator*=(const float &a);
    void operator/=(const float &a);

    float get_norm() const;
    void normalize();
    bool is_ortho(const Vector3 &v);

    /* Vector3 to obj file */
    std::ostream& to_obj(std::ostream& out) const;


    /* Add link to a face */
    void add_face(std::shared_ptr<Face> face);

    /* Remove link to a face */
    void del_face(std::shared_ptr<Face> face);

    /* Is a face related to a vertex */
    bool is_related(std::shared_ptr<Face> face) const;

    /* Compute valence of vertex */
    size_t get_valence() const;

    /* Get the face in common with another vertex,
     * returns nullptr if all different and throw if more than 1 found */
    std::shared_ptr<Face> get_common_face(const Vector3 &v) const;


    friend std::ostream& operator<<(std::ostream &out, const Vector3 &vect);

    // attributes
    float x;
    float y;
    float z;
    std::unordered_set<std::shared_ptr<Face>> related_faces;
    bool is_deleted = false;
    std::string when = "";
};
