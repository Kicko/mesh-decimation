# include "edge_rotation_utils.hh"

std::pair<std::shared_ptr<Face>, std::shared_ptr<Face>>
get_faces_from_edge(std::shared_ptr<Vector3> a, std::shared_ptr<Vector3> b) {
    std::shared_ptr<Face> f1 = nullptr;
    for (auto f : a->related_faces) {
        if (f->is_vertex(b)) {
            f1 = f;
            break;
        }
    }
    std::shared_ptr<Face> f2 = nullptr;
    for (auto f : b->related_faces) {
        if (f->is_vertex(a) && f != f1) {
            f2 = f;
            break;
        }
    }
    return std::make_pair(f1, f2);
}

size_t compute_energy(const std::unordered_set<std::shared_ptr<Vector3>> &hexagon) {
    size_t energy = 0;
    for (auto v : hexagon) {
        energy += std::abs(static_cast<long>(v->get_valence() - 4));
    }
    return energy;
}

std::shared_ptr<Vector3> get_neighbour_on_face(std::shared_ptr<Vector3> b, std::shared_ptr<Face> f) {
    for (int i = 0; i < 4; ++i)
        if (f->points[i] == b)
            return f->points[(i + 2) % 4];
    return nullptr;
}
