//
// Created by hiiro on 4/21/20.
//

# include "face_utils.hh"

std::shared_ptr<Vector3> get_normal_face(std::shared_ptr<Face> f) {
    return std::make_shared<Vector3>(normal(*f->points[0], *f->points[1], *f->points[2]));
}

float get_plan_constant_face(std::shared_ptr<Face> f) {
    auto n = get_normal_face(f);
    return -n->x * f->points[0]->x - n->y * f->points[0]->y - -n->z * f->points[0]->z;
}

float dist_face_point(std::shared_ptr<Face> f, Vector3 v) {
    auto n = get_normal_face(f);
    auto d = get_plan_constant_face(f);
    float num = std::abs(n->x * v.x + n->y * v.y + n->z * v.z + d);
    return num / n->get_norm();
}

bool are_connected(std::shared_ptr<Face> f1, std::shared_ptr<Face> f2) {
    int count = 0;
    for (auto v : f1->points)
        for (auto w : f2->points)
            if (v == w) count++;
    return count == 2;
}

bool connected_to_two_faces(std::shared_ptr<Face> t, std::shared_ptr<Face> f1, std::shared_ptr<Face> f2) {
    return are_connected(t, f1) && are_connected(t, f2);
}

std::unordered_set<std::shared_ptr<Face>> get_relevant_faces(Vector3 origin) {
    std::unordered_set<std::shared_ptr<Face>> faces;
    for (auto f : origin.related_faces)
        faces.insert(f->link_face);
    for (auto f1 : faces) {
        for (auto v : f1->points) {
            for (auto t : v->related_faces) {
                if (t == f1) continue;
                for (auto f2 : faces) {
                    if (f1 == f2) continue;
                    if (connected_to_two_faces(t, f1, f2))
                        faces.insert(t);
                }
            }
        }
    }
    return faces;
}