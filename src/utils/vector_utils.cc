# include "vector_utils.hh"

float distance(const Vector3 &a, const Vector3 &b) {
    return (b - a).get_norm();
}


Vector3 normal(const Vector3 &v0, const Vector3 &v1, const Vector3 &v2) {
    return (v1 - v0).cross(v2 - v0);
}

std::unordered_set<std::shared_ptr<Vector3>> get_1_ring(std::shared_ptr<Vector3> v) {
    std::unordered_set<std::shared_ptr<Vector3>> points;
    for (auto f: v->related_faces) {
        for (int i = 0; i < 4; ++i)
            points.insert(f->points[i]);
    }
    if (points.size() > 0 && !v->is_deleted)
        points.erase(points.find(v));
    return points;
}


Vector3 barycenter_1_ring(std::shared_ptr<Vector3> v) {
    Vector3 b(0, 0, 0);
    auto points = get_1_ring(v);
    for (auto p: points) {
        b += *p;
    }
    return b / points.size();
}

std::set<std::pair<std::shared_ptr<Vector3>, std::shared_ptr<Vector3>>> get_one_ring_edges(std::shared_ptr<Vector3> v) {
    std::set<std::pair<std::shared_ptr<Vector3>, std::shared_ptr<Vector3>>> edges;
    for (auto f : v->related_faces) {
        for (int i = 0; i < 4; ++i)
            if (f->points[i] == v)
            {
                auto p = std::make_pair(f->points[i], f->points[(i + 1) % 4]);
                auto p2 = std::make_pair(f->points[(i + 1) % 4], f->points[i]);
                if (edges.find(p2) == edges.end())
                    edges.insert(p);
                p = std::make_pair(f->points[i], f->points[(i + 3) % 4]);
                p2 = std::make_pair(f->points[(i + 3) % 4], f->points[i]);
                if (edges.find(p2) == edges.end())
                    edges.insert(p);
            }
    }
    return edges;
}
