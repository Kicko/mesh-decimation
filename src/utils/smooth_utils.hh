# pragma once

# include <memory>
# include <utility>
# include <map>
# include <algorithm>

# include "face.hh"
# include "ray.hh"
# include "vector3.hh"
# include "vector_utils.hh"
# include "face_utils.hh"

/* Shoot a ray and stop when touching relevant faces of v */
std::shared_ptr<Vector3> cast_ray(const Ray &ray, const Vector3 &v);

