# include "strings.hh"

std::vector<std::string> my_split(const std::string& s, char delimiter)
{
    std::vector<std::string> tokens;
    std::string token = "";
    long unsigned i = 0;
    while (i < s.length())
    {
        while (i < s.length() && s[i] == delimiter)
            ++i;
        while (i < s.length() && s[i] != delimiter)
            token += s[i++];
        if (token != "")
            tokens.push_back(token);
        token = "";
    }
    return tokens;
}
