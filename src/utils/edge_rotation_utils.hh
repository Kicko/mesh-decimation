//
// Created by hiiro on 4/6/20.
//

# pragma once

# include <unordered_set>

# include "face.hh"
# include "vector3.hh"

Vector3 midpoint(const Vector3 &a, const Vector3 &b);

std::pair<std::shared_ptr<Face>, std::shared_ptr<Face>>
get_faces_from_edge(std::shared_ptr<Vector3> a, std::shared_ptr<Vector3> b);

std::shared_ptr<Vector3> get_neighbour_on_face(std::shared_ptr<Vector3> b, std::shared_ptr<Face> f);

size_t compute_energy(const std::unordered_set<std::shared_ptr<Vector3>> &hexagon);
