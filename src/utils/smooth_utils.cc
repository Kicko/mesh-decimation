# include "smooth_utils.hh"

std::pair<bool, Vector3> intersection_ray_triangle(const Ray &ray,
                                                   std::shared_ptr<Vector3> v0,
                                                   std::shared_ptr<Vector3> v1,
                                                   std::shared_ptr<Vector3> v2) {
    const float eps = 0.0000001;
    auto edge1 = *v1 - *v0;
    auto edge2 = *v2 - *v0;
    auto h = ray.unit.cross(edge2);
    float a = edge1.dot(h);
    if (a > -eps && a < eps)
        return std::make_pair(false, ray.origin);
    auto f = 1. / a;
    auto s = ray.origin - *v0;
    auto u = f * s.dot(h);
    if (u < 0. || u > 1.)
        return std::make_pair(false, ray.origin);
    auto q = s.cross(edge1);
    auto v = f * ray.unit.dot(q);
    if (v < 0. || u + v > 1.)
        return std::make_pair(false, ray.origin);
    float t = f * edge2.dot(q);
    if (t > eps) {
        return std::make_pair(true, ray.origin + ray.unit * t);
    } else {
        return std::make_pair(false, ray.origin);
    }
}

std::shared_ptr<Vector3> cast_ray(const Ray &ray, const Vector3 &v) {
    auto faces = get_relevant_faces(v);
    for (auto f : faces) {
        auto inter = intersection_ray_triangle(ray, f->points[0], f->points[1], f->points[2]);
        if (inter.first)
            return std::make_shared<Vector3>(inter.second);
        inter = intersection_ray_triangle(ray, f->points[1], f->points[2], f->points[3]);
        if (inter.first)
            return std::make_shared<Vector3>(inter.second);
    }
    //std::cerr << "No hit with cast_ray, should not happen\n";
    return nullptr;
}
