# pragma once

# include <utility>
# include <set>

# include "vector3.hh"

float distance(const Vector3 &a, const Vector3 &b);

Vector3 normal(const Vector3 &v1, const Vector3 &v2, const Vector3 &v3);

std::unordered_set<std::shared_ptr<Vector3>> get_1_ring(std::shared_ptr<Vector3> v);

Vector3 barycenter_1_ring(std::shared_ptr<Vector3> v);

std::set<std::pair<std::shared_ptr<Vector3>, std::shared_ptr<Vector3>>> get_one_ring_edges(std::shared_ptr<Vector3> v);

