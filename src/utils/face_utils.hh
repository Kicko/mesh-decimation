//
// Created by hiiro on 4/21/20.
//
# pragma once

# include "face.hh"
# include "vector3.hh"
# include "vector_utils.hh"

/* Return the normal vector of a face */
std::shared_ptr<Vector3> get_normal_face(std::shared_ptr<Face> f);

/* Return the d in ax + by + cz + d = 0 */
float get_plan_constant_face(std::shared_ptr<Face> f);

/* Compute the distance between f and v */
float dist_face_point(std::shared_ptr<Face> f, Vector3 v);

/* Compute if two faces are connected */
bool are_connected(std::shared_ptr<Face> f1, std::shared_ptr<Face> f2);

/* Compute if t is connected to both f1 and f2 */
bool connected_to_two_faces(std::shared_ptr<Face> t, std::shared_ptr<Face> f1, std::shared_ptr<Face> f2);

/* Return the set of relevant face to ray cast from the origin of the ray */
std::unordered_set<std::shared_ptr<Face>> get_relevant_faces(Vector3 origin);
