# include <cmath>
# include "vector3.hh"
# include "strings.hh"

Vector3::Vector3(float x, float y, float z)
    : x(x)
    , y(y)
    , z(z)
{}

Vector3::Vector3(std::string x, std::string y, std::string z)
{
    this->x = std::stof(x);
    this->y = std::stof(y);
    this->z = std::stof(z);
}

bool Vector3::operator==(const Vector3 &v) const
{
    float l = 1;
    if (v.x != 0)
        l = this->x / v.x;
    else if (v.y != 0)
        l = this->y / v.y;
    else if (v.z != 0)
        l = this->z / v.z;
    else
        return true;

    float a = 0.0001;
    return abs(this->x - l * v.x) < a
        && abs(this->y - v.y * l) < a
        && abs(this->z - v.z * l) < a;
}

void Vector3::operator+=(const Vector3 &v)
{
    this->x += v.x;
    this->y += v.y;
    this->z += v.z;
}

void Vector3::operator-=(const Vector3 &v)
{
    this->x -= v.x;
    this->y -= v.y;
    this->z -= v.z;
}

void Vector3::operator*=(const float &a)
{
    this->x *= a;
    this->y *= a;
    this->z *= a;
}

void Vector3::operator/=(const float &a)
{
    this->x /= a;
    this->y /= a;
    this->z /= a;
}

float Vector3::dot(const Vector3 &v) const
{
    return this->x * v.x + this->y * v.y + this->z * v.z;
}

Vector3 Vector3::cross(const Vector3 &v) const
{
    Vector3 ret(0, 0, 0);
    ret.x = this->y * v.z - this->z * v.y;
    ret.y = this->z * v.x - this->x * v.z;
    ret.z = this->x * v.y - this->y * v.x;
    return ret;
}

Vector3 Vector3::operator+(const Vector3 &v) const
{
    Vector3 v2(this->x, this->y, this->z);
    v2 += v;
    return v2;
}

Vector3 Vector3::operator-(const Vector3 &v) const
{
    Vector3 v2(this->x, this->y, this->z);
    v2 -= v;
    return v2;
}

Vector3 Vector3::operator-() const
{
    return Vector3(-this->x, -this->y, -this->z);
}

Vector3 Vector3::operator*(const Vector3 &v) const
{
    Vector3 v2(this->x, this->y, this->z);
    v2.x *= v.x;
    v2.y *= v.y;
    v2.z *= v.z;
    return v2;
}

Vector3 Vector3::operator/(const Vector3 &v) const
{
    Vector3 v2(this->x, this->y, this->z);
    if (v.x == 0)
        v2.x = 0;
    else
        v2.x /= v.x;
    if (v.y == 0)
        v2.y = 0;
    else
        v2.y /= v.y;
    if (v.z == 0)
        v2.z = 0;
    else
        v2.z /= v.z;
    return v2;
}

Vector3 Vector3::operator*(const float &a) const
{
    Vector3 v2(this->x, this->y, this->z);
    v2 *= a;
    return v2;
}

Vector3 Vector3::operator/(const float &a) const
{
    Vector3 v2(this->x, this->y, this->z);
    v2 /= a;
    return v2;
}

float Vector3::get_norm() const
{
    return std::sqrt(this->x * this->x + this->y * this->y + this->z * this->z);
}

void Vector3::normalize()
{
    float norme = this->get_norm();
    this->x /= norme;
    this->y /= norme;
    this->z /= norme;
}

bool Vector3::is_ortho(const Vector3 &v)
{
    return this->dot(v) == 0;
}

std::ostream& Vector3::to_obj(std::ostream& out) const
{
    return out << std::to_string(this->x) << "  " << std::to_string(this->y)
               << "  " << std::to_string(this->z);
}


void Vector3::add_face(std::shared_ptr<Face> face)
{
    if (face != nullptr)
        this->related_faces.insert(face);
}

void Vector3::del_face(std::shared_ptr<Face> face)
{
    auto it = this->related_faces.begin();
    while (it != this->related_faces.end() && *it != face)
        ++it;
    if (it != this->related_faces.end())
        this->related_faces.erase(it);
}

bool Vector3::is_related(std::shared_ptr<Face> face) const
{
    auto it = this->related_faces.begin();
    while (it != this->related_faces.end() && *it != face)
        ++it;
    return it != this->related_faces.end();
}

size_t Vector3::get_valence() const
{
    return this->related_faces.size();
}

std::shared_ptr<Face> Vector3::get_common_face(const Vector3 &v) const
{
    std::shared_ptr<Face> face = nullptr;
    int i = 0;
    for (auto f: this->related_faces)
        if (v.is_related(f))
        {
            if (!face)
                face = f;
            ++i;
        }

    if (i > 2)
        throw std::invalid_argument("too many faces in common");
    return face;
}


std::ostream& operator<<(std::ostream &out, const Vector3 &vect)
{
    out << "Vector 3: (" << vect.x << ", " << vect.y
        << ", " << vect.z << ")";
    return out;
}
