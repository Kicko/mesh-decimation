# pragma once

# include <iostream>
# include <memory>
# include <vector>

class Vector3;

class Face
{
public:
    Face(std::shared_ptr<Vector3> a, std::shared_ptr<Vector3> b,
         std::shared_ptr<Vector3> c, std::shared_ptr<Vector3> d)
    {
        points.push_back(a);
        points.push_back(b);
        points.push_back(c);
        points.push_back(d);
    }

    /* Compare 2 Face */
    bool operator==(const Face &face) const;

    /* Swap a vertex (oldP) with newP */
    void swap(std::shared_ptr<Vector3> oldP, std::shared_ptr<Vector3> newP);

    /* Is a point a vertex of face */
    bool is_vertex(std::shared_ptr<Vector3> point) const;

    /* Get vertices of the smaller diagonal */
    std::pair<std::shared_ptr<Vector3>, std::shared_ptr<Vector3>> get_min_diag() const;

    /* Remove link from vertex to face for all vertices */
    static void cut_links_vertices(std::shared_ptr<Face> face);

    /* Add link to mesh0 face */
    void add_link_mesh0(std::shared_ptr<Face> face);

    friend std::ostream& operator<<(std::ostream &out, const Face &face);


    // attributes
    std::vector<std::shared_ptr<Vector3>> points;

    // link to face on the other mesh
    std::shared_ptr<Face> link_face;
};
