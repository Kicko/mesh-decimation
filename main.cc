# include <iostream>
# include <utils/vector_utils.hh>
# include <boost/program_options.hpp>
# include "3Dobject.hh"
# include "local_operations.hh"
# include "smooth.hh"

namespace po = boost::program_options;

int main(int argc, char* argv[])
{
    std::string input, output;
    int level;
    try {
        po::options_description desc{"Parameters available"};
        desc.add_options()
                ("help,h", "Print this help message")
                ("input,i", po::value<std::string>(), "Path to input obj file")
                ("output,p", po::value<std::string>()->default_value("output.obj"), "Path to output obj file")
                ("level,l", po::value<int>()->default_value(4), "Factor of reduction of the number of vertices");

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);


        if (vm.count("help")) {
            std::cout << desc << '\n';
            return 0;
        }

        if (!vm.count("input")) {
            std::cerr << "Please give an input file with --input flag" << '\n';
            return 2;
        }
        input = vm["input"].as<std::string>();
        output = vm["output"].as<std::string>();
        level = vm["level"].as<int>();
    }
    catch (const std::exception &e) {
        std::cerr << e.what() << '\n';
        return 1;
    }
    Object3D obj;
    std::cout << "Loading obj file...." << std::endl;
    obj.init(input);


    auto size = obj.vertices.size();
    auto nb_v = size / level;
    size_t old_i = 0;
    while (obj.vertices.size() > nb_v)
    {
        auto i = (size - obj.vertices.size()) * 20 / (size - nb_v);
        if (i != old_i)
        {
            for (size_t k = 0; k < i; ++k)
                std::cout << "#";
            std::cout << std::endl;
            old_i = i;
        }
        auto v = LocalOperations::diagonal_collapse(obj);
        if (!v)
            continue;

        auto one_ring = get_1_ring(v);
        for (auto v_tmp : one_ring) {
            LocalOperations::doublet_removal(obj, v_tmp);
            LocalOperations::singlet_removal(obj, v_tmp);
        }
        auto edges = get_one_ring_edges(v);
        for (auto edge : edges)
            LocalOperations::edge_rotation(edge.first, edge.second);
        for (auto v_tmp : one_ring) {
            LocalOperations::doublet_removal(obj, v_tmp);
            LocalOperations::singlet_removal(obj, v_tmp);
        }
        auto new_v = tangent_smooth(v);
        v->x = new_v->x;
        v->y = new_v->y;
        v->z = new_v->z;
    }

    std::cout << "Writing obj file...." << std::endl;
    obj.to_obj(output);


    return 0;
}
