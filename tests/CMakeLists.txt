include_directories(
    ${CMAKE_SOURCE_DIR}/src
    ${CMAKE_SOURCE_DIR}/src/utils
)

add_executable(test_exe EXCLUDE_FROM_ALL
        test_vector.cc
        test_face.cc
        test_utils_string.cc
        test_3Dobject.cc
        test_vertex.cc
        test_utils_vector.cc
        test_compare.cc
        test_diagonal_collapse.cc
        test_removals.cc
        test_edge_rotation.cc
        test_utils_face.cc)
target_link_libraries(test_exe criterion)

# sources
target_link_libraries(test_exe
        lib_src
)

add_custom_target(check
                COMMAND test_exe --verbose
                DEPENDS test_exe
)
