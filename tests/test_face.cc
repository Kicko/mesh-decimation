# include <criterion/criterion.h>
# include "face.hh"
# include "vector3.hh"

Test(face, test_init)
{
    std::shared_ptr<Vector3> v1 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v2 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v3 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v4 = std::make_shared<Vector3>(0, 0, 0);
    Face f(v1, v2, v3, v4);
    cr_assert_eq(f.points[0], v1);
    cr_assert_eq(f.points[1], v2);
    cr_assert_eq(f.points[2], v3);
    cr_assert_eq(f.points[3], v4);
}

Test(face, equal)
{
    std::shared_ptr<Vector3> v1 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v2 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v3 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v4 = std::make_shared<Vector3>(0, 0, 0);
    Face f(v1, v2, v3, v4);
    Face f2(v1, v2, v3, v4);
    cr_assert(f == f2);
}

Test(face, not_equal)
{
    std::shared_ptr<Vector3> v1 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v2 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v3 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v4 = std::make_shared<Vector3>(0, 0, 0);
    Face f(v1, v2, v3, v4);
    std::shared_ptr<Vector3> v5 = std::make_shared<Vector3>(0, 0, 0);
    Face f2(v1, v2, v5, v4);
    cr_assert(!(f == f2));
}

Test(face, swap)
{
    std::shared_ptr<Vector3> v1 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v2 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v3 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v4 = std::make_shared<Vector3>(0, 0, 0);
    Face f(v1, v2, v3, v4);
    std::shared_ptr<Vector3> new_v = std::make_shared<Vector3>(0, 0, 1);
    f.swap(v1, new_v);
    cr_assert_eq(f.points[0], new_v);
    cr_assert_eq(f.points[1], v2);
    cr_assert_eq(f.points[2], v3);
    cr_assert_eq(f.points[3], v4);
}

Test(face, swap2)
{
    std::shared_ptr<Vector3> v1 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v2 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v3 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v4 = std::make_shared<Vector3>(0, 0, 0);
    Face f(v1, v2, v3, v4);
    std::shared_ptr<Vector3> new_v = std::make_shared<Vector3>(0, 0, 1);
    f.swap(v3, new_v);
    cr_assert_eq(f.points[0], v1);
    cr_assert_eq(f.points[1], v2);
    cr_assert_eq(f.points[2], new_v);
    cr_assert_eq(f.points[3], v4);
}

Test(face, swap_fail)
{
    std::shared_ptr<Vector3> v1 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v2 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v3 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v4 = std::make_shared<Vector3>(0, 0, 0);
    Face f(v1, v2, v3, v4);
    std::shared_ptr<Vector3> new_v = std::make_shared<Vector3>(0, 0, 1);
    cr_assert_throw(f.swap(new_v, new_v), std::invalid_argument);
    cr_assert_eq(f.points[0], v1);
    cr_assert_eq(f.points[1], v2);
    cr_assert_eq(f.points[2], v3);
    cr_assert_eq(f.points[3], v4);
}

Test(face, related)
{
    std::shared_ptr<Vector3> v1 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v2 = std::make_shared<Vector3>(0, 0, 1);
    std::shared_ptr<Vector3> v3 = std::make_shared<Vector3>(0, 0, 2);
    std::shared_ptr<Vector3> v4 = std::make_shared<Vector3>(0, 0, 3);
    Face f(v1, v2, v3, v4);
    cr_assert(f.is_vertex(v1));
    cr_assert(f.is_vertex(v2));
    cr_assert(f.is_vertex(v3));
    cr_assert(f.is_vertex(v4));
}

Test(face, not_related)
{
    std::shared_ptr<Vector3> v1 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v2 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v3 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v4 = std::make_shared<Vector3>(0, 0, 0);
    Face f(v1, v2, v3, v4);
    std::shared_ptr<Vector3> new_v = std::make_shared<Vector3>(0, 0, 1);
    std::shared_ptr<Vector3> v0 = std::make_shared<Vector3>(0, 0, 1);
    f.swap(v3, new_v);
    cr_assert(f.is_vertex(v1));
    cr_assert(f.is_vertex(v2));
    cr_assert(!f.is_vertex(v3));
    cr_assert(f.is_vertex(v4));
    cr_assert(!f.is_vertex(v0));
    cr_assert(f.is_vertex(new_v));
}

Test(face, min_diag1)
{
    std::shared_ptr<Vector3> v1 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v2 = std::make_shared<Vector3>(0, 1, 0);
    std::shared_ptr<Vector3> v3 = std::make_shared<Vector3>(0, 1, 1);
    std::shared_ptr<Vector3> v4 = std::make_shared<Vector3>(0, 0, 2);
    Face f(v1, v2, v3, v4);
    auto diag = f.get_min_diag();
    cr_assert_eq(diag.first, v1);
    cr_assert_eq(diag.second, v3);
}

Test(face, min_diag2)
{
    std::shared_ptr<Vector3> v1 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v2 = std::make_shared<Vector3>(0, 1, 0);
    std::shared_ptr<Vector3> v3 = std::make_shared<Vector3>(0, 2, 1);
    std::shared_ptr<Vector3> v4 = std::make_shared<Vector3>(0, 0, 1);
    Face f(v1, v2, v3, v4);
    auto diag = f.get_min_diag();
    cr_assert_eq(diag.first, v2);
    cr_assert_eq(diag.second, v4);
}

Test(face, min_diag_eq)
{
    std::shared_ptr<Vector3> v1 = std::make_shared<Vector3>(0, 0, -1);
    std::shared_ptr<Vector3> v2 = std::make_shared<Vector3>(0, 1, -1);
    std::shared_ptr<Vector3> v3 = std::make_shared<Vector3>(0, 1, 1);
    std::shared_ptr<Vector3> v4 = std::make_shared<Vector3>(0, -1, 1);
    Face f(v1, v2, v3, v4);
    auto diag = f.get_min_diag();
    cr_assert_eq(diag.first, v1);
    cr_assert_eq(diag.second, v3);
}

Test(face, cut_links_vertices)
{
    std::shared_ptr<Vector3> v1 = std::make_shared<Vector3>(0, 0, -1);
    std::shared_ptr<Vector3> v2 = std::make_shared<Vector3>(0, 1, -1);
    std::shared_ptr<Vector3> v3 = std::make_shared<Vector3>(0, 1, 1);
    std::shared_ptr<Vector3> v4 = std::make_shared<Vector3>(0, -1, 1);
    auto f = std::make_shared<Face>(v1, v2, v3, v4);

    Face::cut_links_vertices(f);

    cr_assert(!v1->is_related(f));
    cr_assert(!v2->is_related(f));
    cr_assert(!v3->is_related(f));
    cr_assert(!v4->is_related(f));
}
