# include <criterion/criterion.h>

# include "vector3.hh"

Test(vector3, init)
{
    Vector3 v1(0, 0, 0);
    cr_assert(v1.x == 0);
    cr_assert(v1.y == 0);
    cr_assert(v1.z == 0);
}

Test(Vector3, equal)
{
    Vector3 v1(1, 0, 2);
    Vector3 v2(1, 0, 2);
    cr_assert(v1 == v2, "Not equal");
}

Test(vector3, add)
{
    Vector3 v1(1, 2, 3);
    Vector3 v2(-1, 0, 10.002);
    auto res = v1 + v2;
    Vector3 expect(0, 2, 13.002);
    cr_assert(res == expect, "Add failed");
}

Test(vector3, sub)
{
    Vector3 v1(1, 2, 3);
    Vector3 v2(-1, 0, 10.002);
    auto res = v1 - v2;
    Vector3 expect(2, 2, -6.998);
    cr_assert(res == expect, "Sub failed");
}

Test(vector3, mult_vect)
{
    Vector3 v1(1.02, 2, -3.1);
    Vector3 v2(-2, 0, -4.5);
    auto res = v1 * v2;
    Vector3 expect(-2.04, 0, 13.95);
    cr_assert(res == expect, "Mult failed");
}

Test(vector3, mult)
{
    Vector3 v1(1.02, 2, -3.1);
    auto res = v1 * 3;
    Vector3 expect(3.06, 6, -9.3);
    cr_assert(res == expect, "Mult failed");
}

Test(vector3, div)
{
    Vector3 v1(1.02, 2, -3.1);
    auto res = v1 / 2;
    Vector3 expect(0.51, 1, -1.55);
    cr_assert(res == expect, "Div failed");
}

Test(vector3, dot)
{
    Vector3 v1(1, 1, 1);
    Vector3 v2(1, -1, -1);
    auto expect = -1;
    auto res = v1.dot(v2);
    cr_assert(res == expect, "Dot failed");
}

Test(vector3, cross)
{
    Vector3 v1(1, 0, 0);
    Vector3 v2(0, 1, 0);
    Vector3 expect(0, 0, 1);
    auto res = v1.cross(v2);
    cr_assert(res == expect, "Cross failed");
}

Test(vector3, norm)
{
    Vector3 v1(3, 4, 0);
    auto res = v1.get_norm();
    auto expect = 5;
    cr_assert(res == expect, "get_norm failed");
}

Test(vector3, normalize)
{
    Vector3 v1(3, 4, 0);
    v1.normalize();
    Vector3 expect(0.60, 0.80, 0);
    cr_assert(v1 == expect, "normalize failed");
}

Test(vector3, ortho)
{
    Vector3 v1(1, 0, 0);
    Vector3 v2(0, 1, 0);
    auto res = v1.is_ortho(v2);
    auto expect = true;
    cr_assert(res == expect, "ortho failed");
}

Test(vector3, init_string)
{
    Vector3 v("0.0", "1.2", "-1.0003");
    cr_assert_float_eq(v.x, 0, 0.0000001);
    cr_assert_float_eq(v.y, 1.2, 0.0000001);
    cr_assert_float_eq(v.z, -1.0003, 0.0000001);
}

Test(vector3, div_vector)
{
    Vector3 v1(1.02, 2, -2.25);
    Vector3 v2(-2, 1, -4);
    auto res = v1 / v2;
    Vector3 expect(-0.51, 2, 0.5625);
    cr_assert_eq(res, expect, "div_vector failed");
}
