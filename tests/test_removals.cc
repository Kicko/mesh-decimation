# include <criterion/criterion.h>

# include "local_operations.hh"

Test(doublet_removal, example)
{
    Object3D obj;
    auto v1 = std::make_shared<Vector3>(0.0f,  0.0f,  0.0f);
    auto v2 = std::make_shared<Vector3>(0.0f,  0.0f,  1.0f);
    auto v3 = std::make_shared<Vector3>(0.0f,  0.0f,  2.0f);
    auto v4 = std::make_shared<Vector3>(0.0f,  0.0f,  3.0f);

    auto v5 = std::make_shared<Vector3>(0.0f,  1.0f,  0.0f);
    auto v6 = std::make_shared<Vector3>(0.0f,  1.0f,  1.0f);
    auto v7 = std::make_shared<Vector3>(0.0f,  1.0f,  2.0f);
    auto v8 = std::make_shared<Vector3>(0.0f,  1.0f,  3.0f);

    auto v9 = std::make_shared<Vector3>(0.0f,  2.0f,  0.0f);
    auto v10 = std::make_shared<Vector3>(0.0f,  2.0f,  1.0f);
    auto v11 = std::make_shared<Vector3>(0.0f,  2.0f,  2.0f);
    auto v12 = std::make_shared<Vector3>(0.0f,  2.0f,  3.0f);

    auto v13 = std::make_shared<Vector3>(0.0f,  3.0f,  0.0f);
    auto v14 = std::make_shared<Vector3>(0.0f,  3.0f,  1.0f);
    auto v15 = std::make_shared<Vector3>(0.0f,  3.0f,  2.0f);
    auto v16 = std::make_shared<Vector3>(0.0f,  3.0f,  3.0f);

    auto v17 = std::make_shared<Vector3>(0.0f,  1.5f,  1.5f);

    for (auto v: {v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15, v16, v17})
    {
        obj.vertices.push_back(v);
        obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v));
    }

/*
13 - 14 - 15 - 16
|    |    |    |
9 -- 10 - 11 - 12
|    | 17 |    |
5 -- 6 -- 7 -- 8
|    |    |    |
1 -- 2 -- 3 -- 4
*/

    auto f1 = obj.create_face({"f", "1/1/1",  "2/2/2",  "6/6/6",  "5/5/5"});
    auto f2 = obj.create_face({"f", "2/2/2",  "3/3/3",  "7/7/7",  "6/6/6"});
    auto f3 = obj.create_face({"f", "3/3/3",  "4/4/4",  "8/8/8",  "7/7/7"});

    auto f5 = obj.create_face({"f", "5/5/5",  "6/6/6",  "10/10/10",  "9/9/9"});
    auto f6 = obj.create_face({"f", "6/6/6",  "17/17/17",  "11/11/11",  "10/10/10"});
    auto f7 = obj.create_face({"f", "6/6/6",  "7/7/7",  "11/11/11", "17/17/17"});
    auto f8 = obj.create_face({"f", "7/7/7",  "8/8/8",  "12/12/12",  "11/11/11"});

    auto f10 = obj.create_face({"f", "9/9/9",  "10/10/10",  "14/14/14",  "13/13/13"});
    auto f11 = obj.create_face({"f", "10/10/10",  "11/11/11",  "15/15/15",  "14/14/14"});
    auto f12 = obj.create_face({"f", "11/11/11",  "12/12/12",  "16/16/16",  "15/15/15"});

    for (auto f: {f1, f2, f3, f5, f6, f7, f8, f10, f11, f12})
        obj.faces.push_back(f);

    obj.link_vertices_to_face();

    obj.add_all_to_heap();

    LocalOperations::doublet_removal(obj, v17);

    cr_assert_eq(obj.faces.size(), 9);
    cr_assert_eq(obj.vertices.size(), 16);
    bool e1 = std::find(obj.faces.begin(), obj.faces.end(), f6) == obj.faces.end();
    bool e2 = std::find(obj.faces.begin(), obj.faces.end(), f7) == obj.faces.end();
    if (!e1)
        cr_assert(e2, "face not removed");
    if (!e2)
        cr_assert(e1, "face not removed");
    cr_assert_eq(std::find(obj.vertices.begin(), obj.vertices.end(), v17), obj.vertices.end());

    std::shared_ptr<Face> f = (e2 ? f6 : f7);
    for (auto v: {v6, v7, v10, v11})
    {
        cr_assert(f->is_vertex(v));
        cr_assert(v->is_related(f));
    }
}

Test(doublet_removal, example2)
{
    Object3D obj;
    auto v1 = std::make_shared<Vector3>(0.0f,  0.0f,  0.0f);
    auto v2 = std::make_shared<Vector3>(0.0f,  0.0f,  1.0f);
    auto v3 = std::make_shared<Vector3>(0.0f,  0.0f,  2.0f);
    auto v4 = std::make_shared<Vector3>(0.0f,  0.0f,  3.0f);

    auto v5 = std::make_shared<Vector3>(0.0f,  1.0f,  0.0f);
    auto v6 = std::make_shared<Vector3>(0.0f,  1.0f,  1.0f);
    auto v7 = std::make_shared<Vector3>(0.0f,  1.0f,  2.0f);
    auto v8 = std::make_shared<Vector3>(0.0f,  1.0f,  3.0f);

    auto v9 = std::make_shared<Vector3>(0.0f,  2.0f,  0.0f);
    auto v10 = std::make_shared<Vector3>(0.0f,  2.0f,  1.0f);
    auto v11 = std::make_shared<Vector3>(0.0f,  2.0f,  2.0f);
    auto v12 = std::make_shared<Vector3>(0.0f,  2.0f,  3.0f);

    auto v13 = std::make_shared<Vector3>(0.0f,  3.0f,  0.0f);
    auto v14 = std::make_shared<Vector3>(0.0f,  3.0f,  1.0f);
    auto v15 = std::make_shared<Vector3>(0.0f,  3.0f,  2.0f);
    auto v16 = std::make_shared<Vector3>(0.0f,  3.0f,  3.0f);

    auto v17 = std::make_shared<Vector3>(0.0f,  1.5f,  1.5f);

    for (auto v: {v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15, v16, v17})
    {
        obj.vertices.push_back(v);
        obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v));
    }

/*
13 - 14 - 15 - 16
|    |    |    |
9 -- 10 - 11 - 12
|    | 17 |    |
5 -- 6 -- 7 -- 8
|    |    |    |
1 -- 2 -- 3 -- 4
*/

    auto f1 = obj.create_face({"f", "1/1/1",  "2/2/2",  "6/6/6",  "5/5/5"});
    auto f2 = obj.create_face({"f", "2/2/2",  "3/3/3",  "7/7/7",  "6/6/6"});
    auto f3 = obj.create_face({"f", "3/3/3",  "4/4/4",  "8/8/8",  "7/7/7"});

    auto f5 = obj.create_face({"f", "5/5/5",  "6/6/6",  "10/10/10",  "9/9/9"});
    auto f6 = obj.create_face({"f", "6/6/6",  "7/7/7",  "17/17/17",  "10/10/10"});
    auto f7 = obj.create_face({"f", "7/7/7",  "11/11/11", "10/10/10", "17/17/17"});
    auto f8 = obj.create_face({"f", "7/7/7",  "8/8/8",  "12/12/12",  "11/11/11"});

    auto f10 = obj.create_face({"f", "9/9/9",  "10/10/10",  "14/14/14",  "13/13/13"});
    auto f11 = obj.create_face({"f", "10/10/10",  "11/11/11",  "15/15/15",  "14/14/14"});
    auto f12 = obj.create_face({"f", "11/11/11",  "12/12/12",  "16/16/16",  "15/15/15"});

    for (auto f: {f1, f2, f3, f5, f6, f7, f8, f10, f11, f12})
        obj.faces.push_back(f);

    obj.link_vertices_to_face();

    obj.add_all_to_heap();

    LocalOperations::doublet_removal(obj, v17);

    cr_assert_eq(obj.faces.size(), 9);
    cr_assert_eq(obj.vertices.size(), 16);
    bool e1 = std::find(obj.faces.begin(), obj.faces.end(), f6) == obj.faces.end();
    bool e2 = std::find(obj.faces.begin(), obj.faces.end(), f7) == obj.faces.end();
    if (!e1)
        cr_assert(e2, "face not removed");
    if (!e2)
        cr_assert(e1, "face not removed");
    cr_assert_eq(std::find(obj.vertices.begin(), obj.vertices.end(), v17), obj.vertices.end());

    std::shared_ptr<Face> f = (e1 ? f7 : f6);
    for (auto v: {v6, v7, v10, v11})
    {
        cr_assert(f->is_vertex(v));
        cr_assert(v->is_related(f));
    }
}

Test(doublet_removal, recursion1)
{
    Object3D obj;
    auto v1 = std::make_shared<Vector3>(0.0f,  0.0f,  0.0f);
    auto v2 = std::make_shared<Vector3>(0.0f,  0.0f,  1.0f);
    auto v3 = std::make_shared<Vector3>(0.0f,  0.0f,  2.0f);
    auto v4 = std::make_shared<Vector3>(0.0f,  0.0f,  3.0f);

    auto v5 = std::make_shared<Vector3>(0.0f,  1.0f,  0.0f);
    auto v6 = std::make_shared<Vector3>(0.0f,  1.0f,  1.0f);
    auto v7 = std::make_shared<Vector3>(0.0f,  1.0f,  2.0f);
    auto v8 = std::make_shared<Vector3>(0.0f,  1.0f,  3.0f);

    auto v9 = std::make_shared<Vector3>(0.0f,  2.0f,  0.0f);
    auto v10 = std::make_shared<Vector3>(0.0f,  2.0f,  1.0f);
    auto v11 = std::make_shared<Vector3>(0.0f,  2.0f,  2.0f);
    auto v12 = std::make_shared<Vector3>(0.0f,  2.0f,  3.0f);

    auto v13 = std::make_shared<Vector3>(0.0f,  3.0f,  0.0f);
    auto v14 = std::make_shared<Vector3>(0.0f,  3.0f,  1.0f);
    auto v15 = std::make_shared<Vector3>(0.0f,  3.0f,  2.0f);
    auto v16 = std::make_shared<Vector3>(0.0f,  3.0f,  3.0f);

    auto v17 = std::make_shared<Vector3>(0.0f,  1.5f,  1.3f);
    auto v18 = std::make_shared<Vector3>(0.0f,  1.5f,  1.7f);

    for (auto v: {v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15, v16, v17, v18})
    {
        obj.vertices.push_back(v);
        obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v));
    }

/*
13 - 14 - 15 - 16
|    |    |    |
9 -- 10 - 11 - 12
|    |17-18|    |
5 -- 6 -- 7 -- 8
|    |    |    |
1 -- 2 -- 3 -- 4
*/

    auto f1 = obj.create_face({"f", "1/1/1",  "2/2/2",  "6/6/6",  "5/5/5"});
    auto f2 = obj.create_face({"f", "2/2/2",  "3/3/3",  "7/7/7",  "6/6/6"});
    auto f3 = obj.create_face({"f", "3/3/3",  "4/4/4",  "8/8/8",  "7/7/7"});

    auto f5 = obj.create_face({"f", "5/5/5",  "6/6/6",  "10/10/10",  "9/9/9"});
    auto f6 = obj.create_face({"f", "6/6/6",  "17/17/17",  "11/11/11",  "10/10/10"});
    auto f13 = obj.create_face({"f", "6/6/6",  "17/17/17",  "11/11/11",  "18/18/18"});
    auto f7 = obj.create_face({"f", "6/6/6",  "7/7/7",  "11/11/11", "18/18/18"});
    auto f8 = obj.create_face({"f", "7/7/7",  "8/8/8",  "12/12/12",  "11/11/11"});

    auto f10 = obj.create_face({"f", "9/9/9",  "10/10/10",  "14/14/14",  "13/13/13"});
    auto f11 = obj.create_face({"f", "10/10/10",  "11/11/11",  "15/15/15",  "14/14/14"});
    auto f12 = obj.create_face({"f", "11/11/11",  "12/12/12",  "16/16/16",  "15/15/15"});

    for (auto f: {f1, f2, f3, f5, f6, f7, f8, f10, f11, f12, f13})
        obj.faces.push_back(f);

    obj.link_vertices_to_face();

    obj.add_all_to_heap();

    LocalOperations::doublet_removal(obj, v17);

    cr_assert_eq(obj.faces.size(), 9);
    cr_assert_eq(obj.vertices.size(), 16);
    bool e1 = std::find(obj.faces.begin(), obj.faces.end(), f6) == obj.faces.end();
    bool e2 = std::find(obj.faces.begin(), obj.faces.end(), f7) == obj.faces.end();
    bool e3 = std::find(obj.faces.begin(), obj.faces.end(), f13) == obj.faces.end();
    if (!e1)
        cr_assert(e2 && e3, "face not removed");
    if (!e2)
        cr_assert(e1 && e3, "face not removed");
    if (!e3)
        cr_assert(e1 && e2, "face not removed");
    cr_assert_eq(std::find(obj.vertices.begin(), obj.vertices.end(), v17), obj.vertices.end());
    cr_assert_eq(std::find(obj.vertices.begin(), obj.vertices.end(), v18), obj.vertices.end());

    std::shared_ptr<Face> f = (e1 ? (e2 ? f13 : f7) : f6);
    for (auto v: {v6, v7, v10, v11})
    {
        cr_assert(f->is_vertex(v));
        cr_assert(v->is_related(f));
    }
}

Test(singlet_removal, example)
{
    Object3D obj;
    auto v1 = std::make_shared<Vector3>(0.0f,  0.0f,  0.0f);
    auto v2 = std::make_shared<Vector3>(0.0f,  0.0f,  1.0f);
    auto v3 = std::make_shared<Vector3>(0.0f,  0.0f,  2.0f);
    auto v4 = std::make_shared<Vector3>(0.0f,  0.0f,  3.0f);
    auto v5 = std::make_shared<Vector3>(0.0f,  0.0f,  4.0f);

    auto v6 = std::make_shared<Vector3>(0.0f,  1.0f,  0.0f);
    auto v7 = std::make_shared<Vector3>(0.0f,  1.0f,  1.0f);
    auto v8 = std::make_shared<Vector3>(0.0f,  1.0f,  2.0f);
    auto v9 = std::make_shared<Vector3>(0.0f,  1.0f,  3.0f);
    auto v10 = std::make_shared<Vector3>(0.0f,  1.0f,  4.0f);

    auto v11 = std::make_shared<Vector3>(0.0f,  2.0f,  0.0f);
    auto v12 = std::make_shared<Vector3>(0.0f,  2.0f,  1.0f);
    auto v13 = std::make_shared<Vector3>(0.0f,  2.0f,  2.0f);
    auto v14 = std::make_shared<Vector3>(0.0f,  2.0f,  3.0f);
    auto v15 = std::make_shared<Vector3>(0.0f,  2.0f,  4.0f);

    auto v16 = std::make_shared<Vector3>(0.0f,  3.0f,  0.0f);
    auto v17 = std::make_shared<Vector3>(0.0f,  3.0f,  1.0f);
    auto v18 = std::make_shared<Vector3>(0.0f,  3.0f,  2.0f);
    auto v19 = std::make_shared<Vector3>(0.0f,  3.0f,  3.0f);
    auto v20 = std::make_shared<Vector3>(0.0f,  3.0f,  4.0f);

    auto v21 = std::make_shared<Vector3>(0.0f,  1.5f,  1.5f);
    auto v22 = std::make_shared<Vector3>(0.0f,  1.0f,  2.0f);

    for (auto v: {v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,
                  v15, v16, v17, v18, v19, v20, v21, v22})
    {
        obj.vertices.push_back(v);
        obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v));
    }

/*
16 - 17 - 18 - 19 - 20
|    |    |    |    |
11 - 12 - 13 - 14 - 15
|    |   / \   |    |
|    |  | 21\  |    |
|    |  | /\|  |    |
6 -- 7 -(8 22)-9 -- 10
|    |    |    |    |
1 -- 2 -- 3 -- 4 -- 5
*/

    auto f1 = obj.create_face({"f", "1/1/1",  "2/2/2",  "7/7/7",  "6/6/6"});
    auto f2 = obj.create_face({"f", "2/2/2",  "3/3/3",  "8/8/8",  "7/7/7"});
    auto f3 = obj.create_face({"f", "3/3/3",  "4/4/4",  "9/9/9",  "8/8/8"});
    auto f4 = obj.create_face({"f", "4/4/4",  "5/5/5",  "10/10/10",  "9/9/9"});

    auto f5 = obj.create_face({"f", "6/6/6",  "7/7/7",  "12/12/12", "11/11/11"});
    auto f6 = obj.create_face({"f", "7/7/7",  "8/8/8",  "13/13/13", "12/12/12"});
    auto f7 = obj.create_face({"f", "8/8/8",  "13/13/13", "22/22/22", "21/21/21"});
    auto f8 = obj.create_face({"f", "22/22/22", "9/9/9", "14/14/14", "13/13/13"});
    auto f9 = obj.create_face({"f", "9/9/9",  "10/10/10", "15/15/15", "14/14/14"});

    auto f10 = obj.create_face({"f", "11/11/11",  "12/12/12",  "17/17/17", "16/16/16"});
    auto f11 = obj.create_face({"f", "12/12/12",  "13/13/13",  "18/18/18", "17/17/17"});
    auto f12 = obj.create_face({"f", "13/13/13", "14/14/14", "19/19/19", "18/18/18"});
    auto f13 = obj.create_face({"f", "14/14/14", "15/15/15", "20/20/20", "19/19/19"});

    for (auto f: {f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13})
        obj.faces.push_back(f);

    obj.link_vertices_to_face();

    obj.add_all_to_heap();

    LocalOperations::singlet_removal(obj, v21);

    cr_assert_eq(obj.faces.size(), 12);
    cr_assert_eq(obj.vertices.size(), 20);
    cr_assert_eq(std::find(obj.faces.begin(), obj.faces.end(), f7), obj.faces.end());
    cr_assert_eq(std::find(obj.vertices.begin(), obj.vertices.end(), v21), obj.vertices.end());
    cr_assert_eq(std::find(obj.vertices.begin(), obj.vertices.end(), v22), obj.vertices.end());

    for (auto f: {f6, f8, f2, f3})
        cr_assert_neq(std::find(v8->related_faces.begin(), v8->related_faces.end(), f), v8->related_faces.end());

    for (auto f: {f6, f8, f11, f12})
        cr_assert_neq(std::find(v13->related_faces.begin(), v13->related_faces.end(), f), v13->related_faces.end());

    for (auto v: {v7, v8, v12, v13})
        cr_assert(f6->is_vertex(v));
    for (auto v: {v9, v8, v14, v13})
        cr_assert(f8->is_vertex(v));
}
