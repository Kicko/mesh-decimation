# include <criterion/criterion.h>

# include "vector_utils.hh"

Test(vector_utils, distance0)
{
    Vector3 a(0, 0, 0);
    Vector3 b(0, 0, 0);
    float f = distance(a, b);
    cr_assert_eq(f, 0);
}

Test(vector_utils, distance_same)
{
    Vector3 a(0, 0, 0);
    float f = distance(a, a);
    cr_assert_eq(f, 0);
}

Test(vector_utils, distance1_neg)
{
    Vector3 a(0, 0, 0);
    Vector3 b(0, 0, -1);
    float f = distance(a, b);
    cr_assert_eq(f, 1);
}

Test(vector_utils, distance2)
{
    Vector3 a(2, 0, 0);
    Vector3 b(4, 0, 0);
    float f = distance(a, b);
    cr_assert_eq(f, 2);
}

Test(vector_utils, distance_diag)
{
    Vector3 a(1, 1, 1);
    Vector3 b(0, 0, 0);
    float f = distance(a, b);
    cr_assert_float_eq(f, 1.732051, 0.00001);
}

Test(vertor_utils, normal_z)
{
    Vector3 a(0, 0, 0);
    Vector3 b(0, 0, 1);
    Vector3 c(0, 1, 0);

    cr_assert_eq(normal(a, b, c), Vector3(1, 0, 0));
}

Test(vertor_utils, normal_y)
{
    Vector3 a(0, 0, 0);
    Vector3 b(0, 0, 1);
    Vector3 c(1, 0, 0);

    cr_assert_eq(normal(a, b, c), Vector3(0, 1, 0));
}

Test(vertor_utils, normal_x)
{
    Vector3 a(0, 0, 0);
    Vector3 b(1, 0, 0);
    Vector3 c(0, 1, 0);

    cr_assert_eq(normal(a, b, c), Vector3(0, 0, 1));
}

Test(vertor_utils, normal_medium)
{
    Vector3 a(1, 2, -2);
    Vector3 b(-1, 3, 1);
    Vector3 c(2, 0, -2);

    cr_assert_eq(normal(a, b, c), Vector3(2, 1, 1));
}

// test get_1_ring

// test barycenter_1_ring
