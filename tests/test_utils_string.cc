# include <criterion/criterion.h>

# include "strings.hh"

Test(strings, my_split_empty)
{
    std::vector<std::string> s = my_split("", ' ');
    cr_assert_eq(s.size(), 0);
}

Test(strings, my_split_simple)
{
    std::vector<std::string> s = my_split("a", ' ');
    cr_assert_eq(s.size(), 1);
    cr_assert(s[0] == "a");
}

Test(strings, my_split_long)
{
    std::vector<std::string> s = my_split("abcde", ' ');
    cr_assert_eq(s.size(), 1);
    cr_assert(s[0] == "abcde");
}

Test(strings, my_split_double_simple)
{
    std::vector<std::string> s = my_split("a b", ' ');
    cr_assert_eq(s.size(), 2);
    cr_assert(s[0] == "a");
    cr_assert(s[1] == "b");
}

Test(strings, my_split_double_long)
{
    std::vector<std::string> s = my_split("abcde qwerty", ' ');
    cr_assert_eq(s.size(), 2);
    cr_assert(s[0] == "abcde");
    cr_assert(s[1] == "qwerty");
}

Test(strings, my_split_multiple_simple)
{
    std::vector<std::string> s = my_split("a b c d e f g h i j k", ' ');
    cr_assert_eq(s.size(), 11);
    cr_assert(s[0] == "a");
    cr_assert(s[1] == "b");
    cr_assert(s[2] == "c");
    cr_assert(s[3] == "d");
    cr_assert(s[4] == "e");
    cr_assert(s[5] == "f");
    cr_assert(s[6] == "g");
    cr_assert(s[7] == "h");
    cr_assert(s[8] == "i");
    cr_assert(s[9] == "j");
    cr_assert(s[10] == "k");
}

Test(strings, my_split_double_away)
{
    std::vector<std::string> s = my_split("abcde                qwerty", ' ');
    cr_assert_eq(s.size(), 2);
    cr_assert(s[0] == "abcde");
    cr_assert(s[1] == "qwerty");
}

Test(strings, my_split_triple_away)
{
    std::vector<std::string> s = my_split("abcde                qwerty    o.p", ' ');
    cr_assert_eq(s.size(), 3);
    cr_assert(s[0] == "abcde");
    cr_assert(s[1] == "qwerty");
    cr_assert(s[2] == "o.p");
}
