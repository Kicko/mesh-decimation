# include <criterion/criterion.h>

# include "3Dobject.hh"

Test(object3D, link_vertices_to_face)
{
    Object3D obj;
    cr_assert_eq(obj.vertex_to_link.size(), 0);

    auto a = std::make_shared<Vector3>(1.0, 0.5, -5.3);
    auto b = std::make_shared<Vector3>(5.0, 0.5, -5.3);
    auto c = std::make_shared<Vector3>(5.0, -0.5, -5.3);
    auto d = std::make_shared<Vector3>(1.0, -0.5, -5.3);
    obj.vertices.push_back(a);
    obj.vertices.push_back(b);
    obj.vertices.push_back(c);
    obj.vertices.push_back(d);
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*a));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*b));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*c));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*d));
    std::shared_ptr<Face> f = std::make_shared<Face>(a, b, c, d);

    for (int i = 0; i < 4; ++i)
        cr_assert_eq(obj.vertices[i]->get_valence(), 0);

    for (int i = 0; i < 4; ++i)
        obj.vertex_to_link.push_back(std::make_pair(i + 1, f));

    obj.link_vertices_to_face();

    for (int i = 0; i < 4; ++i)
    {
        cr_assert_eq(obj.vertices[i]->get_valence(), 1);
        cr_assert_neq(obj.vertices[i]->related_faces.find(f), obj.vertices[i]->related_faces.end());
    }
}

Test(object3D, create_face)
{
    Object3D obj;
    auto a = std::make_shared<Vector3>(1.0, 0.5, -5.3);
    auto b = std::make_shared<Vector3>(5.0, 0.5, -5.3);
    auto c = std::make_shared<Vector3>(5.0, -0.5, -5.3);
    auto d = std::make_shared<Vector3>(1.0, -0.5, -5.3);
    obj.vertices.push_back(a);
    obj.vertices.push_back(b);
    obj.vertices.push_back(c);
    obj.vertices.push_back(d);
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*a));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*b));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*c));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*d));
    std::shared_ptr<Face> f = obj.create_face({"f", "1/1/1", "2/2/2", "3/3/3", "4/4/4"});
    cr_assert_eq(f->points[0], a);
    cr_assert_eq(f->points[1], b);
    cr_assert_eq(f->points[2], c);
    cr_assert_eq(f->points[3], d);

    cr_assert_eq(obj.vertex_to_link.size(), 4);
    for (int i = 0; i < 4; ++i)
        cr_assert_eq(obj.vertex_to_link[i].second, f);
}

Test(object3D, init)
{
    Object3D obj;
    obj.init("../../3DObjects/cube.obj");

    Vector3 points[] = {
        Vector3(0, 0, 0),
        Vector3(0, 0, 1),
        Vector3(0, 1, 0),
        Vector3(0, 1, 1),
        Vector3(1, 0, 0),
        Vector3(1, 0, 1),
        Vector3(1, 1, 0),
        Vector3(1, 1, 1)
    };

    for (size_t i = 0; i < 8; ++i)
    {
        cr_assert_eq(*obj.vertices[i], points[i]);
    }

    cr_assert_eq(*obj.faces[0]->points[0], points[1 - 1]);
    cr_assert_eq(*obj.faces[0]->points[1], points[3 - 1]);
    cr_assert_eq(*obj.faces[0]->points[2], points[7 - 1]);
    cr_assert_eq(*obj.faces[0]->points[3], points[5 - 1]);
    cr_assert_eq(*obj.faces[1]->points[0], points[1 - 1]);
    cr_assert_eq(*obj.faces[1]->points[1], points[2 - 1]);
    cr_assert_eq(*obj.faces[1]->points[2], points[4 - 1]);
    cr_assert_eq(*obj.faces[1]->points[3], points[3 - 1]);
    cr_assert_eq(*obj.faces[2]->points[0], points[3 - 1]);
    cr_assert_eq(*obj.faces[2]->points[1], points[7 - 1]);
    cr_assert_eq(*obj.faces[2]->points[2], points[8 - 1]);
    cr_assert_eq(*obj.faces[2]->points[3], points[4 - 1]);
    cr_assert_eq(*obj.faces[3]->points[0], points[5 - 1]);
    cr_assert_eq(*obj.faces[3]->points[1], points[7 - 1]);
    cr_assert_eq(*obj.faces[3]->points[2], points[8 - 1]);
    cr_assert_eq(*obj.faces[3]->points[3], points[6 - 1]);
    cr_assert_eq(*obj.faces[4]->points[0], points[1 - 1]);
    cr_assert_eq(*obj.faces[4]->points[1], points[5 - 1]);
    cr_assert_eq(*obj.faces[4]->points[2], points[6 - 1]);
    cr_assert_eq(*obj.faces[4]->points[3], points[2 - 1]);
    cr_assert_eq(*obj.faces[5]->points[0], points[2 - 1]);
    cr_assert_eq(*obj.faces[5]->points[1], points[6 - 1]);
    cr_assert_eq(*obj.faces[5]->points[2], points[8 - 1]);
    cr_assert_eq(*obj.faces[5]->points[3], points[4 - 1]);

    for (auto v : obj.vertices)
        cr_assert_eq(v->get_valence(), 3);
}

Test(object3D, copy)
{
    Object3D obj;
    obj.init("../../3DObjects/cube.obj");

    Object3D obj2(obj);

    cr_assert_eq(obj.vertices.size(), obj2.vertices.size());
    for (size_t i = 0; i < obj.vertices.size(); ++i)
        cr_assert_eq(obj.vertices[i], obj2.vertices[i]);

    cr_assert_eq(obj.vertices_texture.size(), obj2.vertices_texture.size());
    for (size_t i = 0; i < obj.vertices_texture.size(); ++i)
        cr_assert_eq(obj.vertices_texture[i], obj2.vertices_texture[i]);

    cr_assert_eq(obj.vertices_normale.size(), obj2.vertices_normale.size());
    for (size_t i = 0; i < obj.vertices_normale.size(); ++i)
        cr_assert_eq(obj.vertices_normale[i], obj2.vertices_normale[i]);

    cr_assert_eq(obj.faces.size(), obj2.faces.size());
    for (size_t i = 0; i < obj.faces.size(); ++i)
        cr_assert_eq(obj.faces[i], obj2.faces[i]);
}

Test(object3D, equal)
{
    Object3D obj;
    obj.init("../../3DObjects/cube.obj");

    Object3D obj2(obj);

    cr_assert(obj == obj2);
}

Test(object3D, not_equal)
{
    Object3D obj;
    obj.init("../../3DObjects/cube.obj");

    Object3D obj2;
    obj2.init("../../3DObjects/16834_hand_v1_NEW.obj");

    cr_assert(!(obj == obj2));
}

Test(object3D, to_obj)
{
    Object3D obj;
    obj.init("../../3DObjects/cube.obj");
    obj.to_obj("output.obj");
    Object3D obj2;
    obj2.init("output.obj");
}

Test(object3D, remove_face)
{
    Object3D obj;
    auto a = std::make_shared<Vector3>(1.0, 0.5, -5.3);
    auto b = std::make_shared<Vector3>(5.0, 0.5, -5.3);
    auto c = std::make_shared<Vector3>(5.0, -0.5, -5.3);
    auto d = std::make_shared<Vector3>(1.0, -0.5, -5.3);
    obj.vertices.push_back(a);
    obj.vertices.push_back(b);
    obj.vertices.push_back(c);
    obj.vertices.push_back(d);
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*a));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*b));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*c));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*d));
    std::shared_ptr<Face> f = obj.create_face({"f", "1/1/1", "2/2/2", "3/3/3", "4/4/4"});
    obj.faces.push_back(f);
    obj.link_vertices_to_face();

    a->del_face(f);
    b->del_face(f);
    c->del_face(f);
    d->del_face(f);

    cr_assert_no_throw(obj.remove_face(f), std::invalid_argument);
}

Test(object3D, not_remove_face)
{
    Object3D obj;
    auto a = std::make_shared<Vector3>(1.0, 0.5, -5.3);
    auto b = std::make_shared<Vector3>(5.0, 0.5, -5.3);
    auto c = std::make_shared<Vector3>(5.0, -0.5, -5.3);
    auto d = std::make_shared<Vector3>(1.0, -0.5, -5.3);
    obj.vertices.push_back(a);
    obj.vertices.push_back(b);
    obj.vertices.push_back(c);
    obj.vertices.push_back(d);
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*a));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*b));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*c));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*d));
    std::shared_ptr<Face> f = obj.create_face({"f", "1/1/1", "2/2/2", "3/3/3", "4/4/4"});
    obj.faces.push_back(f);
    obj.link_vertices_to_face();

    cr_assert_throw(obj.remove_face(f), std::invalid_argument);
}

Test(object3D, not_remove_face2)
{
    Object3D obj;
    auto a = std::make_shared<Vector3>(1.0, 0.5, -5.3);
    auto b = std::make_shared<Vector3>(5.0, 0.5, -5.3);
    auto c = std::make_shared<Vector3>(5.0, -0.5, -5.3);
    auto d = std::make_shared<Vector3>(1.0, -0.5, -5.3);
    obj.vertices.push_back(a);
    obj.vertices.push_back(b);
    obj.vertices.push_back(c);
    obj.vertices.push_back(d);
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*a));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*b));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*c));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*d));
    std::shared_ptr<Face> f = obj.create_face({"f", "1/1/1", "2/2/2", "3/3/3", "4/4/4"});
    obj.faces.push_back(f);
    obj.link_vertices_to_face();

    auto v1 = std::make_shared<Vector3>(0, 0, 0);
    auto v2 = std::make_shared<Vector3>(0, 0, 1);
    auto v3 = std::make_shared<Vector3>(0, 0, 2);
    auto v4 = std::make_shared<Vector3>(0, 0, 3);
    auto face = std::make_shared<Face>(v1, v2, v3, v4);

    cr_assert_throw(obj.remove_face(face), std::invalid_argument);
}

Test(object3D, remove_vertex)
{
    Object3D obj;
    auto a = std::make_shared<Vector3>(1.0, 0.5, -5.3);
    auto b = std::make_shared<Vector3>(5.0, 0.5, -5.3);
    auto c = std::make_shared<Vector3>(5.0, -0.5, -5.3);
    auto d = std::make_shared<Vector3>(1.0, -0.5, -5.3);
    obj.vertices.push_back(a);
    obj.vertices.push_back(b);
    obj.vertices.push_back(c);
    obj.vertices.push_back(d);
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*a));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*b));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*c));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*d));
    std::shared_ptr<Face> f = obj.create_face({"f", "1/1/1", "2/2/2", "3/3/3", "4/4/4"});
    obj.faces.push_back(f);
    obj.link_vertices_to_face();

    auto v = std::make_shared<Vector3>(1.0, -0.5, 0);
    f->swap(b, v);
    obj.vertices.push_back(v);

    cr_assert_no_throw(obj.remove_vertex(b), std::invalid_argument);
}

Test(object3D, not_remove_vertex)
{
    Object3D obj;
    auto a = std::make_shared<Vector3>(1.0, 0.5, -5.3);
    auto b = std::make_shared<Vector3>(5.0, 0.5, -5.3);
    auto c = std::make_shared<Vector3>(5.0, -0.5, -5.3);
    auto d = std::make_shared<Vector3>(1.0, -0.5, -5.3);
    obj.vertices.push_back(a);
    obj.vertices.push_back(b);
    obj.vertices.push_back(c);
    obj.vertices.push_back(d);
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*a));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*b));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*c));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*d));
    std::shared_ptr<Face> f = obj.create_face({"f", "1/1/1", "2/2/2", "3/3/3", "4/4/4"});
    obj.faces.push_back(f);
    obj.link_vertices_to_face();

    auto v = std::make_shared<Vector3>(1.0, -0.5, 0);

    cr_assert_throw(obj.remove_vertex(v), std::invalid_argument);
}

Test(object3D, not_remove_vertex2)
{
    Object3D obj;
    auto a = std::make_shared<Vector3>(1.0, 0.5, -5.3);
    auto b = std::make_shared<Vector3>(5.0, 0.5, -5.3);
    auto c = std::make_shared<Vector3>(5.0, -0.5, -5.3);
    auto d = std::make_shared<Vector3>(1.0, -0.5, -5.3);
    obj.vertices.push_back(a);
    obj.vertices.push_back(b);
    obj.vertices.push_back(c);
    obj.vertices.push_back(d);
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*a));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*b));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*c));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*d));
    std::shared_ptr<Face> f = obj.create_face({"f", "1/1/1", "2/2/2", "3/3/3", "4/4/4"});
    obj.faces.push_back(f);
    obj.link_vertices_to_face();

    cr_assert_throw(obj.remove_vertex(a), std::invalid_argument);
}

Test(object3D, init_heap1)
{
    Object3D obj;
    auto a = std::make_shared<Vector3>(1.0, 0.5, -5.3);
    auto b = std::make_shared<Vector3>(5.0, 0.5, -5.3);
    auto c = std::make_shared<Vector3>(5.0, -0.5, -5.3);
    auto d = std::make_shared<Vector3>(1.0, -0.5, -5.3);
    obj.vertices.push_back(a);
    obj.vertices.push_back(b);
    obj.vertices.push_back(c);
    obj.vertices.push_back(d);
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*a));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*b));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*c));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*d));
    std::shared_ptr<Face> f = obj.create_face({"f", "1/1/1", "2/2/2", "3/3/3", "4/4/4"});
    obj.faces.push_back(f);
    obj.link_vertices_to_face();

    cr_assert_eq(obj.diag_heap.size(), 0);

    obj.add_all_to_heap();

    cr_assert_eq(obj.diag_heap.size(), 1);
    cr_assert_eq(obj.diag_heap.top(), f);
}

Test(object3D, init_heap2)
{
    Object3D obj;
    auto a = std::make_shared<Vector3>(1.0, 0.5, 0);
    auto b = std::make_shared<Vector3>(5.0, 0.5, 0);
    auto c = std::make_shared<Vector3>(5.0, -0.5, 0);
    auto d = std::make_shared<Vector3>(1.0, -0.5, 0);
    obj.vertices.push_back(a);
    obj.vertices.push_back(b);
    obj.vertices.push_back(c);
    obj.vertices.push_back(d);
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*a));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*b));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*c));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*d));
    std::shared_ptr<Face> f = obj.create_face({"f", "1/1/1", "2/2/2", "3/3/3", "4/4/4"});
    obj.faces.push_back(f);

    auto g = std::make_shared<Vector3>(0, -0.5, 0);
    auto e = std::make_shared<Vector3>(0, 0.5, 0);
    obj.vertices.push_back(g);
    obj.vertices.push_back(e);
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*g));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*e));
    std::shared_ptr<Face> f2 = obj.create_face({"f", "1/1/1", "4/4/4", "5/5/5", "6/6/6"});
    obj.faces.push_back(f2);

    obj.link_vertices_to_face();

    cr_assert_eq(obj.diag_heap.size(), 0);

    obj.add_all_to_heap();

    cr_assert_eq(obj.diag_heap.size(), 2);
    cr_assert_eq(obj.diag_heap.top(), f2);
    obj.diag_heap.pop();
    cr_assert_eq(obj.diag_heap.top(), f);
    obj.diag_heap.pop();
    cr_assert_eq(obj.diag_heap.size(), 0);
}

Test(object3D, init_heap3)
{
    Object3D obj;
    auto a = std::make_shared<Vector3>(1.0, 0.5, 0);
    auto b = std::make_shared<Vector3>(5.0, 0.5, 0);
    auto c = std::make_shared<Vector3>(5.0, -0.5, 0);
    auto d = std::make_shared<Vector3>(1.0, -0.5, 0);
    obj.vertices.push_back(a);
    obj.vertices.push_back(b);
    obj.vertices.push_back(c);
    obj.vertices.push_back(d);
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*a));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*b));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*c));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*d));
    std::shared_ptr<Face> f = obj.create_face({"f", "1/1/1", "2/2/2", "3/3/3", "4/4/4"});
    obj.faces.push_back(f);

    auto g = std::make_shared<Vector3>(0, -0.5, 0);
    auto e = std::make_shared<Vector3>(0, 0.5, 0);
    obj.vertices.push_back(g);
    obj.vertices.push_back(e);
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*g));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*e));
    std::shared_ptr<Face> f2 = obj.create_face({"f", "1/1/1", "4/4/4", "5/5/5", "6/6/6"});
    obj.faces.push_back(f2);

    auto h = std::make_shared<Vector3>(-2, 0.5, 0);
    auto i = std::make_shared<Vector3>(-2, -0.5, 0);
    obj.vertices.push_back(h);
    obj.vertices.push_back(i);
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*h));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*i));
    std::shared_ptr<Face> f3 = obj.create_face({"f", "5/5/5", "6/6/6", "7/7/7", "8/8/8"});
    obj.faces.push_back(f3);

    obj.link_vertices_to_face();

    cr_assert_eq(obj.diag_heap.size(), 0);

    obj.add_all_to_heap();

    cr_assert_eq(obj.diag_heap.size(), 3);
    cr_assert_eq(obj.diag_heap.top(), f2);
    obj.diag_heap.pop();
    cr_assert_eq(obj.diag_heap.top(), f3);
    obj.diag_heap.pop();
    cr_assert_eq(obj.diag_heap.top(), f);
    obj.diag_heap.pop();
    cr_assert_eq(obj.diag_heap.size(), 0);
}

Test(object3D, barycenter)
{
    Object3D obj;
    obj.init("../../3DObjects/cube.obj");

    cr_assert_eq(obj.get_barycenter(), Vector3(0.5f, 0.5f, 0.5f));
}

Test(object3D, barycenter2)
{
    Object3D obj;
    auto a = std::make_shared<Vector3>(1.0, 0.5, 0);
    auto b = std::make_shared<Vector3>(5.0, 0.5, 0);
    auto c = std::make_shared<Vector3>(5.0, -0.5, 0);
    auto d = std::make_shared<Vector3>(1.0, -0.5, 0);
    obj.vertices.push_back(a);
    obj.vertices.push_back(b);
    obj.vertices.push_back(c);
    obj.vertices.push_back(d);
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*a));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*b));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*c));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*d));

    cr_assert_eq(obj.get_barycenter(), Vector3(3, 0, 0));
}

Test(object3D, bounding_box)
{
    Object3D obj;
    auto a = std::make_shared<Vector3>(1.0, 0.51, 0);
    auto b = std::make_shared<Vector3>(5.0, 5.5, 0.6);
    auto c = std::make_shared<Vector3>(-5.0, -6.53, 9);
    auto d = std::make_shared<Vector3>(10.0, -0.5, 0);
    auto e = std::make_shared<Vector3>(12.0, -0.5, -0.01);

    for (auto v: {a, b, c, d, e})
    {
        obj.vertices.push_back(v);
        obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v));
    }

    auto bbox = obj.bounding_box();

    cr_assert_eq(bbox.first, Vector3(-5, -6.53, -0.01));
    cr_assert_eq(bbox.second, Vector3(12, 5.5, 9));
}

// test init mesh0 + add_link_mesh0
