# include <criterion/criterion.h>

# include "vector3.hh"

Test(vertex, valence0)
{
    Vector3 v(1, 2, 3);
    cr_assert_eq(v.get_valence(), 0);
}

Test(vertex, add_face)
{
    Vector3 v(1, 2, 3);
    auto f = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    v.add_face(f);
    cr_assert_eq(v.related_faces.size(), 1);
    cr_assert_eq(v.get_valence(), 1);
    cr_assert_neq(v.related_faces.find(f), v.related_faces.end());
}

Test(vertex, add_face_null)
{
    Vector3 v(1, 2, 3);
    std::shared_ptr<Face> f = nullptr;
    v.add_face(f);
    cr_assert_eq(v.get_valence(), 0);
}

Test(vertex, add_face_3)
{
    Vector3 v(1, 2, 3);
    auto f = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f2 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f3 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    v.add_face(f);
    v.add_face(f2);
    v.add_face(f3);
    cr_assert_eq(v.get_valence(), 3);
    cr_assert_neq(v.related_faces.find(f), v.related_faces.end());
    cr_assert_neq(v.related_faces.find(f2), v.related_faces.end());
    cr_assert_neq(v.related_faces.find(f3), v.related_faces.end());
}

Test(vertex, del_face)
{
    Vector3 v(1, 2, 3);
    auto f = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f2 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f3 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    v.add_face(f);
    v.add_face(f2);
    v.add_face(f3);
    v.del_face(f2);
    cr_assert_eq(v.get_valence(), 2);
    cr_assert_neq(v.related_faces.find(f), v.related_faces.end());
    cr_assert_neq(v.related_faces.find(f3), v.related_faces.end());
    cr_assert_eq(v.related_faces.find(f2), v.related_faces.end());
}

Test(vertex, del_face_null)
{
    Vector3 v(1, 2, 3);
    auto f = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f2 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f3 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    v.add_face(f);
    v.add_face(f2);
    v.add_face(f3);
    v.del_face(nullptr);
    cr_assert_eq(v.get_valence(), 3);
    cr_assert_neq(v.related_faces.find(f), v.related_faces.end());
    cr_assert_neq(v.related_faces.find(f2), v.related_faces.end());
    cr_assert_neq(v.related_faces.find(f3), v.related_faces.end());
}

Test(vertex, del_face_not_exist)
{
    Vector3 v(1, 2, 3);
    auto f = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f2 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    v.add_face(f);
    v.del_face(f2);
    cr_assert_eq(v.get_valence(), 1);
    cr_assert_neq(v.related_faces.find(f), v.related_faces.end());
}

Test(vertex, del_face_all)
{
    Vector3 v(1, 2, 3);
    auto f = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f2 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f3 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    v.add_face(f);
    v.add_face(f2);
    v.add_face(f3);
    v.del_face(f2);
    v.del_face(f);
    v.del_face(f3);
    cr_assert_eq(v.get_valence(), 0);
}

Test(vertex, del_face_double)
{
    Vector3 v(1, 2, 3);
    auto f = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f2 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f3 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    v.add_face(f);
    v.add_face(f2);
    v.add_face(f3);
    v.del_face(f2);
    v.del_face(f);
    v.del_face(f);
    cr_assert_eq(v.get_valence(), 1);
    cr_assert_neq(v.related_faces.find(f3), v.related_faces.end());
}

Test(vertex, related)
{
    Vector3 v(1, 2, 3);
    auto f = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f2 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f3 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    v.add_face(f);
    v.add_face(f2);
    v.add_face(f3);
    cr_assert(v.is_related(f));
    cr_assert(v.is_related(f2));
    cr_assert(v.is_related(f3));
}

Test(vertex, not_related)
{
    Vector3 v(1, 2, 3);
    auto f = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f2 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f3 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    v.add_face(f);
    v.add_face(f2);
    v.del_face(f2);
    cr_assert(v.is_related(f));
    cr_assert(!v.is_related(f2));
    cr_assert(!v.is_related(f3));
}

Test(vertex, common1)
{
    Vector3 v(1, 2, 3);
    auto f = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f2 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f3 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    v.add_face(f);
    v.add_face(f2);
    v.add_face(f3);

    Vector3 v2(1, 2, 4);
    auto f4 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f5 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    v2.add_face(f3);
    v2.add_face(f4);
    v2.add_face(f5);

    cr_assert_eq(v.get_common_face(v2), f3);
}

Test(vertex, common2)
{
    Vector3 v(1, 2, 3);
    auto f = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f2 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f3 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    v.add_face(f);
    v.add_face(f2);
    v.add_face(f3);

    Vector3 v2(1, 2, 4);
    auto f4 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    v2.add_face(f3);
    v2.add_face(f4);
    v2.add_face(f2);

    cr_assert_throw(v.get_common_face(v2), std::invalid_argument);
}

Test(vertex, no_common)
{
    Vector3 v(1, 2, 3);
    auto f = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f2 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f3 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    v.add_face(f);
    v.add_face(f2);
    v.add_face(f3);

    Vector3 v2(1, 2, 4);
    auto f4 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f5 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    auto f6 = std::make_shared<Face>(nullptr, nullptr, nullptr, nullptr);
    v2.add_face(f4);
    v2.add_face(f5);
    v2.add_face(f6);

    cr_assert_eq(v.get_common_face(v2), nullptr);
}
