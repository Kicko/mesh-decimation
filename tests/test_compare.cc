# include <criterion/criterion.h>

# include "compare.hh"
# include "vector3.hh"

Test(compare, same)
{
    std::shared_ptr<Vector3> v1 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v2 = std::make_shared<Vector3>(0, 1, 0);
    std::shared_ptr<Vector3> v3 = std::make_shared<Vector3>(0, 1, 1);
    std::shared_ptr<Vector3> v4 = std::make_shared<Vector3>(0, 0, 1);
    auto f1 = std::make_shared<Face>(v1, v2, v3, v4);

    std::shared_ptr<Vector3> v5 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v6 = std::make_shared<Vector3>(0, -1, 0);
    std::shared_ptr<Vector3> v7 = std::make_shared<Vector3>(0, -1, -1);
    std::shared_ptr<Vector3> v8 = std::make_shared<Vector3>(0, 0, -1);
    auto f2 = std::make_shared<Face>(v5, v6, v7, v8);

    cr_assert(!CompareDiagonal()(f1, f2));
}

Test(compare, great)
{
    std::shared_ptr<Vector3> v1 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v2 = std::make_shared<Vector3>(0, 2, 0);
    std::shared_ptr<Vector3> v3 = std::make_shared<Vector3>(0, 2, 3);
    std::shared_ptr<Vector3> v4 = std::make_shared<Vector3>(0, 0, 3);
    auto f1 = std::make_shared<Face>(v1, v2, v3, v4);

    std::shared_ptr<Vector3> v5 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v6 = std::make_shared<Vector3>(0, -1, 0);
    std::shared_ptr<Vector3> v7 = std::make_shared<Vector3>(0, -1, -1);
    std::shared_ptr<Vector3> v8 = std::make_shared<Vector3>(0, 0, -1);
    auto f2 = std::make_shared<Face>(v5, v6, v7, v8);

    cr_assert(CompareDiagonal()(f1, f2));
}

Test(compare, small)
{
    std::shared_ptr<Vector3> v1 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v2 = std::make_shared<Vector3>(0, 1, 0);
    std::shared_ptr<Vector3> v3 = std::make_shared<Vector3>(0, 1, 1);
    std::shared_ptr<Vector3> v4 = std::make_shared<Vector3>(0, 0, 1);
    auto f1 = std::make_shared<Face>(v1, v2, v3, v4);

    std::shared_ptr<Vector3> v5 = std::make_shared<Vector3>(0, 0, 0);
    std::shared_ptr<Vector3> v6 = std::make_shared<Vector3>(0, -2, 0);
    std::shared_ptr<Vector3> v7 = std::make_shared<Vector3>(0, -2, -1.5);
    std::shared_ptr<Vector3> v8 = std::make_shared<Vector3>(0, 0, -1.5);
    auto f2 = std::make_shared<Face>(v5, v6, v7, v8);

    cr_assert(!CompareDiagonal()(f1, f2));
}
