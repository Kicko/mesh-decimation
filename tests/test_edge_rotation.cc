# include <criterion/criterion.h>

# include "local_operations.hh"
# include "edge_rotation_utils.hh"

Test(edge_rotation, compute_energy)
{
    auto v1 = std::make_shared<Vector3>(0.0f, 1.0f, 0.0f);
    auto v2 = std::make_shared<Vector3>(0.5f, 0.5f, 0.0f);
    auto v3 = std::make_shared<Vector3>(0.5f, -0.5f, 0.0f);
    auto v4 = std::make_shared<Vector3>(0.0f, -1.0f, 0.0f);
    auto v5 = std::make_shared<Vector3>(-0.5f, -0.5f, 0.0f);
    auto v6 = std::make_shared<Vector3>(-0.5f, 0.5f, 0.0f);

    auto f1 = std::make_shared<Face>(v1, v2, v3, v4);
    auto f2 = std::make_shared<Face>(v1, v4, v5, v6);
    v1->add_face(f1);
    v1->add_face(f2);
    v4->add_face(f1);
    v4->add_face(f2);
    v2->add_face(f1);
    v3->add_face(f1);
    v5->add_face(f2);
    v6->add_face(f2);

    std::unordered_set<std::shared_ptr<Vector3>> hexagon;
    hexagon.insert(v1);
    hexagon.insert(v2);
    hexagon.insert(v3);
    hexagon.insert(v4);
    hexagon.insert(v5);
    hexagon.insert(v6);

    cr_assert_eq(compute_energy(hexagon), 16);
}

Test(edge_rotation, get_faces_from_edges)
{
    auto v1 = std::make_shared<Vector3>(0.0f, 1.0f, 0.0f);
    auto v2 = std::make_shared<Vector3>(0.5f, 0.5f, 0.0f);
    auto v3 = std::make_shared<Vector3>(0.5f, -0.5f, 0.0f);
    auto v4 = std::make_shared<Vector3>(0.0f, -1.0f, 0.0f);
    auto v5 = std::make_shared<Vector3>(-0.5f, -0.5f, 0.0f);
    auto v6 = std::make_shared<Vector3>(-0.5f, 0.5f, 0.0f);

    auto f1 = std::make_shared<Face>(v1, v2, v3, v4);
    auto f2 = std::make_shared<Face>(v1, v4, v5, v6);
    v1->add_face(f1);
    v1->add_face(f2);
    v4->add_face(f1);
    v4->add_face(f2);
    v2->add_face(f1);
    v3->add_face(f1);
    v5->add_face(f2);
    v6->add_face(f2);

    auto p = get_faces_from_edge(v1, v4);
    cr_assert_eq(p.first, f2);
    cr_assert_eq(p.second, f1);
}

Test(edge_rotation, get_neighbour_on_face)
{
    auto v1 = std::make_shared<Vector3>(0.0f, 1.0f, 0.0f);
    auto v2 = std::make_shared<Vector3>(0.5f, 0.5f, 0.0f);
    auto v3 = std::make_shared<Vector3>(0.5f, -0.5f, 0.0f);
    auto v4 = std::make_shared<Vector3>(0.0f, -1.0f, 0.0f);
    auto v5 = std::make_shared<Vector3>(-0.5f, -0.5f, 0.0f);
    auto v6 = std::make_shared<Vector3>(-0.5f, 0.5f, 0.0f);

    auto f1 = std::make_shared<Face>(v1, v2, v3, v4);
    auto f2 = std::make_shared<Face>(v1, v4, v5, v6);

    cr_assert_eq(get_neighbour_on_face(v4, f1), v2);
    cr_assert_eq(get_neighbour_on_face(v1, f1), v3);
    cr_assert_eq(get_neighbour_on_face(v4, f2), v6);
    cr_assert_eq(get_neighbour_on_face(v1, f2), v5);
}


Test(edge_rotation, example)
{
    Object3D obj;
    auto v1 = std::make_shared<Vector3>(0.0f,  0.0f,  0.0f);
    auto v2 = std::make_shared<Vector3>(0.0f,  0.0f,  1.0f);
    auto v3 = std::make_shared<Vector3>(0.0f,  0.0f,  2.0f);
    auto v4 = std::make_shared<Vector3>(0.0f,  0.0f,  3.0f);
    auto v5 = std::make_shared<Vector3>(0.0f,  0.0f,  4.0f);

    auto v6 = std::make_shared<Vector3>(0.0f,  1.0f,  0.0f);
    auto v7 = std::make_shared<Vector3>(0.0f,  1.0f,  1.0f);
    auto v8 = std::make_shared<Vector3>(0.0f,  1.0f,  2.0f);
    auto v9 = std::make_shared<Vector3>(0.0f,  1.0f,  3.0f);
    auto v10 = std::make_shared<Vector3>(0.0f,  1.0f,  4.0f);

    auto v11 = std::make_shared<Vector3>(0.0f,  2.0f,  0.0f);
    auto v12 = std::make_shared<Vector3>(0.0f,  2.0f,  1.0f);
    auto v13 = std::make_shared<Vector3>(0.0f,  2.0f,  2.0f);
    auto v14 = std::make_shared<Vector3>(0.0f,  2.0f,  3.0f);
    auto v15 = std::make_shared<Vector3>(0.0f,  2.0f,  4.0f);

    auto v16 = std::make_shared<Vector3>(0.0f,  3.0f,  0.0f);
    auto v17 = std::make_shared<Vector3>(0.0f,  3.0f,  1.0f);
    auto v18 = std::make_shared<Vector3>(0.0f,  3.0f,  2.0f);
    auto v19 = std::make_shared<Vector3>(0.0f,  3.0f,  3.0f);
    auto v20 = std::make_shared<Vector3>(0.0f,  3.0f,  4.0f);

    for (auto v: {v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13,
                  v14, v15, v16, v17, v18, v19, v20})
    {
        obj.vertices.push_back(v);
        obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v));
    }

    auto f1 = obj.create_face({"f", "1/1/1",  "2/2/2",  "7/7/7",  "6/6/6"});
    auto f2 = obj.create_face({"f", "2/2/2",  "3/3/3",  "8/8/8",  "7/7/7"});
    auto f3 = obj.create_face({"f", "3/3/3",  "4/4/4",  "9/9/9",  "8/8/8"});
    auto f4 = obj.create_face({"f", "4/4/4",  "5/5/5",  "10/10/10",  "9/9/9"});

    auto f5 = obj.create_face({"f", "6/6/6",  "7/7/7",  "12/12/12",  "11/11/11"});
    auto f6 = obj.create_face({"f", "7/7/7",  "8/8/8",  "13/13/13",  "12/12/12"});
    auto f7 = obj.create_face({"f", "8/8/8",  "9/9/9",  "14/14/14",  "13/13/13"});
    auto f8 = obj.create_face({"f", "9/9/9",  "10/10/10",  "15/15/15",  "14/14/14"});

    auto f9 = obj.create_face({"f", "11/11/11",  "12/12/12",  "17/17/17",  "16/16/16"});
    auto f10 = obj.create_face({"f", "12/12/12",  "13/13/13",  "18/18/18",  "17/17/17"});
    auto f11 = obj.create_face({"f", "13/13/13",  "14/14/14",  "19/19/19",  "18/18/18"});
    auto f12 = obj.create_face({"f", "14/14/14",  "15/15/15",  "20/20/20",  "19/19/19"});

    for (auto f: {f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12})
        obj.faces.push_back(f);

    obj.link_vertices_to_face();

    obj.add_all_to_heap();

    auto faces_v8 = v8->related_faces;
    auto faces_v13 = v13->related_faces;

    LocalOperations::edge_rotation(v8, v13);

    cr_assert_eq(obj.faces.size(), 12);
    cr_assert_eq(obj.vertices.size(), 20);
    cr_assert_eq(v8->related_faces.size(), 4);
    cr_assert_eq(v13->related_faces.size(), 4);

    for (auto f : faces_v8)
    {
        cr_assert_neq(std::find(v8->related_faces.begin(), v8->related_faces.end(), f),
                  v8->related_faces.end());
    }
    for (auto f : faces_v13)
    {
        cr_assert_neq(std::find(v13->related_faces.begin(), v13->related_faces.end(), f),
                  v13->related_faces.end());
    }

    cr_assert(f6->is_vertex(v7));
    cr_assert(f6->is_vertex(v8));
    cr_assert(f6->is_vertex(v12));
    cr_assert(f6->is_vertex(v13));

    cr_assert(f7->is_vertex(v8));
    cr_assert(f7->is_vertex(v9));
    cr_assert(f7->is_vertex(v13));
    cr_assert(f7->is_vertex(v14));
}

Test(edge_rotation, example2)
{
    Object3D obj;
    auto v1 = std::make_shared<Vector3>(0.0f,  0.0f,  0.0f);
    auto v2 = std::make_shared<Vector3>(0.0f,  0.0f,  1.0f);
    auto v3 = std::make_shared<Vector3>(0.0f,  0.0f,  2.0f);
    auto v4 = std::make_shared<Vector3>(0.0f,  0.0f,  3.0f);
    auto v5 = std::make_shared<Vector3>(0.0f,  0.0f,  4.0f);

    auto v6 = std::make_shared<Vector3>(0.0f,  1.0f,  0.0f);
    auto v7 = std::make_shared<Vector3>(0.0f,  1.0f,  1.0f);
    auto v8 = std::make_shared<Vector3>(0.0f,  1.0f,  2.0f);
    auto v9 = std::make_shared<Vector3>(0.0f,  1.0f,  3.0f);
    auto v10 = std::make_shared<Vector3>(0.0f,  1.0f,  4.0f);

    auto v11 = std::make_shared<Vector3>(0.0f,  2.0f,  0.0f);
    auto v12 = std::make_shared<Vector3>(0.0f,  2.0f,  1.0f);
    auto v13 = std::make_shared<Vector3>(0.0f,  2.0f,  2.0f);
    auto v14 = std::make_shared<Vector3>(0.0f,  2.0f,  3.0f);
    auto v15 = std::make_shared<Vector3>(0.0f,  2.0f,  4.0f);

    auto v16 = std::make_shared<Vector3>(0.0f,  3.0f,  0.0f);
    auto v17 = std::make_shared<Vector3>(0.0f,  3.0f,  1.0f);
    auto v18 = std::make_shared<Vector3>(0.0f,  3.0f,  2.0f);
    auto v19 = std::make_shared<Vector3>(0.0f,  3.0f,  3.0f);
    auto v20 = std::make_shared<Vector3>(0.0f,  3.0f,  4.0f);

    for (auto v: {v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13,
                  v14, v15, v16, v17, v18, v19, v20})
    {
        obj.vertices.push_back(v);
        obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v));
    }

    auto f1 = obj.create_face({"f", "1/1/1",  "2/2/2",  "7/7/7",  "6/6/6"});
    auto f2 = obj.create_face({"f", "2/2/2",  "3/3/3",  "8/8/8",  "7/7/7"});
    auto f3 = obj.create_face({"f", "3/3/3",  "4/4/4",  "9/9/9",  "8/8/8"});
    auto f4 = obj.create_face({"f", "4/4/4",  "5/5/5",  "10/10/10",  "9/9/9"});

    auto f5 = obj.create_face({"f", "6/6/6",  "7/7/7",  "12/12/12",  "11/11/11"});
    auto f6 = obj.create_face({"f", "7/7/7",  "14/14/14",  "13/13/13",  "12/12/12"});
    auto f7 = obj.create_face({"f", "7/7/7", "8/8/8",  "9/9/9",  "14/14/14"});
    auto f8 = obj.create_face({"f", "9/9/9",  "10/10/10",  "15/15/15",  "14/14/14"});

    auto f9 = obj.create_face({"f", "11/11/11",  "12/12/12",  "17/17/17",  "16/16/16"});
    auto f10 = obj.create_face({"f", "12/12/12",  "13/13/13",  "18/18/18",  "17/17/17"});
    auto f11 = obj.create_face({"f", "13/13/13",  "14/14/14",  "19/19/19",  "18/18/18"});
    auto f12 = obj.create_face({"f", "14/14/14",  "15/15/15",  "20/20/20",  "19/19/19"});

    for (auto f: {f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12})
        obj.faces.push_back(f);

    obj.link_vertices_to_face();

    obj.add_all_to_heap();

    LocalOperations::edge_rotation(v7, v14);

    cr_assert_eq(obj.faces.size(), 12);
    cr_assert_eq(obj.vertices.size(), 20);
    cr_assert_eq(v7->related_faces.size(), 4);
    cr_assert_eq(v14->related_faces.size(), 4);

    cr_assert_neq(std::find(v7->related_faces.begin(), v7->related_faces.end(), f1),
              v7->related_faces.end());
    cr_assert_neq(std::find(v7->related_faces.begin(), v7->related_faces.end(), f2),
              v7->related_faces.end());
    cr_assert_neq(std::find(v7->related_faces.begin(), v7->related_faces.end(), f5),
              v7->related_faces.end());
    cr_assert_neq(std::find(v7->related_faces.begin(), v7->related_faces.end(), f6),
              v7->related_faces.end());

    cr_assert_neq(std::find(v14->related_faces.begin(), v14->related_faces.end(), f7),
              v14->related_faces.end());
    cr_assert_neq(std::find(v14->related_faces.begin(), v14->related_faces.end(), f8),
              v14->related_faces.end());
    cr_assert_neq(std::find(v14->related_faces.begin(), v14->related_faces.end(), f11),
              v14->related_faces.end());
    cr_assert_neq(std::find(v14->related_faces.begin(), v14->related_faces.end(), f12),
              v14->related_faces.end());

    cr_assert(f6->is_vertex(v7));
    cr_assert(f6->is_vertex(v8));
    cr_assert(f6->is_vertex(v12));
    cr_assert(f6->is_vertex(v13));
    cr_assert(!f6->is_vertex(v14));

    cr_assert(f7->is_vertex(v8));
    cr_assert(f7->is_vertex(v9));
    cr_assert(f7->is_vertex(v13));
    cr_assert(f7->is_vertex(v14));
    cr_assert(!f7->is_vertex(v7));
}

Test(edge_rotation, example3)
{
    Object3D obj;
    auto v1 = std::make_shared<Vector3>(0.0f,  0.0f,  0.0f);
    auto v2 = std::make_shared<Vector3>(0.0f,  0.0f,  1.0f);
    auto v3 = std::make_shared<Vector3>(0.0f,  0.0f,  2.0f);
    auto v4 = std::make_shared<Vector3>(0.0f,  0.0f,  3.0f);
    auto v5 = std::make_shared<Vector3>(0.0f,  0.0f,  4.0f);

    auto v6 = std::make_shared<Vector3>(0.0f,  1.0f,  0.0f);
    auto v7 = std::make_shared<Vector3>(0.0f,  1.0f,  1.0f);
    auto v8 = std::make_shared<Vector3>(0.0f,  1.0f,  2.0f);
    auto v9 = std::make_shared<Vector3>(0.0f,  1.0f,  3.0f);
    auto v10 = std::make_shared<Vector3>(0.0f,  1.0f,  4.0f);

    auto v11 = std::make_shared<Vector3>(0.0f,  2.0f,  0.0f);
    auto v12 = std::make_shared<Vector3>(0.0f,  2.0f,  1.0f);
    auto v13 = std::make_shared<Vector3>(0.0f,  2.0f,  2.0f);
    auto v14 = std::make_shared<Vector3>(0.0f,  2.0f,  3.0f);
    auto v15 = std::make_shared<Vector3>(0.0f,  2.0f,  4.0f);

    auto v16 = std::make_shared<Vector3>(0.0f,  3.0f,  0.0f);
    auto v17 = std::make_shared<Vector3>(0.0f,  3.0f,  1.0f);
    auto v18 = std::make_shared<Vector3>(0.0f,  3.0f,  2.0f);
    auto v19 = std::make_shared<Vector3>(0.0f,  3.0f,  3.0f);
    auto v20 = std::make_shared<Vector3>(0.0f,  3.0f,  4.0f);

    for (auto v: {v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13,
                  v14, v15, v16, v17, v18, v19, v20})
    {
        obj.vertices.push_back(v);
        obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v));
    }

    auto f1 = obj.create_face({"f", "1/1/1",  "2/2/2",  "7/7/7",  "6/6/6"});
    auto f2 = obj.create_face({"f", "2/2/2",  "3/3/3",  "8/8/8",  "7/7/7"});
    auto f3 = obj.create_face({"f", "3/3/3",  "4/4/4",  "9/9/9",  "8/8/8"});
    auto f4 = obj.create_face({"f", "4/4/4",  "5/5/5",  "10/10/10",  "9/9/9"});

    auto f5 = obj.create_face({"f", "6/6/6",  "7/7/7",  "12/12/12",  "11/11/11"});
    auto f6 = obj.create_face({"f", "7/7/7",  "8/8/8",  "9/9/9",  "12/12/12"});
    auto f7 = obj.create_face({"f", "9/9/9",  "14/14/14", "13/13/13", "12/12/12"});
    auto f8 = obj.create_face({"f", "9/9/9",  "10/10/10",  "15/15/15",  "14/14/14"});

    auto f9 = obj.create_face({"f", "11/11/11",  "12/12/12",  "17/17/17",  "16/16/16"});
    auto f10 = obj.create_face({"f", "12/12/12",  "13/13/13",  "18/18/18",  "17/17/17"});
    auto f11 = obj.create_face({"f", "13/13/13",  "14/14/14",  "19/19/19",  "18/18/18"});
    auto f12 = obj.create_face({"f", "14/14/14",  "15/15/15",  "20/20/20",  "19/19/19"});

    for (auto f: {f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12})
        obj.faces.push_back(f);

    obj.link_vertices_to_face();

    obj.add_all_to_heap();

    LocalOperations::edge_rotation(v9, v12);

    cr_assert_eq(obj.faces.size(), 12);
    cr_assert_eq(obj.vertices.size(), 20);
    cr_assert_eq(v9->related_faces.size(), 4);
    cr_assert_eq(v12->related_faces.size(), 4);

    cr_assert_neq(std::find(v9->related_faces.begin(), v9->related_faces.end(), f3),
              v9->related_faces.end());
    cr_assert_neq(std::find(v9->related_faces.begin(), v9->related_faces.end(), f4),
              v9->related_faces.end());
    cr_assert_neq(std::find(v9->related_faces.begin(), v9->related_faces.end(), f7),
              v9->related_faces.end());
    cr_assert_neq(std::find(v9->related_faces.begin(), v9->related_faces.end(), f8),
              v9->related_faces.end());

    cr_assert_neq(std::find(v12->related_faces.begin(), v12->related_faces.end(), f5),
              v12->related_faces.end());
    cr_assert_neq(std::find(v12->related_faces.begin(), v12->related_faces.end(), f6),
              v12->related_faces.end());
    cr_assert_neq(std::find(v12->related_faces.begin(), v12->related_faces.end(), f9),
              v12->related_faces.end());
    cr_assert_neq(std::find(v12->related_faces.begin(), v12->related_faces.end(), f10),
              v12->related_faces.end());

    cr_assert(f6->is_vertex(v7));
    cr_assert(f6->is_vertex(v8));
    cr_assert(f6->is_vertex(v12));
    cr_assert(f6->is_vertex(v13));
    cr_assert(!f6->is_vertex(v9));

    cr_assert(f7->is_vertex(v8));
    cr_assert(f7->is_vertex(v9));
    cr_assert(f7->is_vertex(v13));
    cr_assert(f7->is_vertex(v14));
    cr_assert(!f7->is_vertex(v12));
}
