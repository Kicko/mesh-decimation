# include <criterion/criterion.h>

# include "local_operations.hh"

Test(diagonal_collapse, example)
{
    Object3D obj;
    auto v1 = std::make_shared<Vector3>(0.0f,  0.0f,  0.0f);
    auto v2 = std::make_shared<Vector3>(0.0f,  0.0f,  1.0f);
    auto v3 = std::make_shared<Vector3>(0.0f,  0.0f,  2.0f);
    auto v4 = std::make_shared<Vector3>(0.0f,  0.0f,  3.0f);
    auto v5 = std::make_shared<Vector3>(0.0f,  0.0f,  4.0f);

    auto v6 = std::make_shared<Vector3>(0.0f,  1.0f,  0.0f);
    auto v7 = std::make_shared<Vector3>(0.0f,  1.0f,  1.0f);
    auto v8 = std::make_shared<Vector3>(0.0f,  1.0f,  2.0f);
    auto v9 = std::make_shared<Vector3>(0.0f,  1.0f,  3.0f);
    auto v10 = std::make_shared<Vector3>(0.0f,  1.0f,  4.0f);

    auto v11 = std::make_shared<Vector3>(0.0f,  2.0f,  0.0f);
    auto v12 = std::make_shared<Vector3>(0.0f,  2.0f,  1.0f);
    auto v13 = std::make_shared<Vector3>(0.0f,  2.0f,  1.8f);
    auto v14 = std::make_shared<Vector3>(0.0f,  2.0f,  3.0f);
    auto v15 = std::make_shared<Vector3>(0.0f,  2.0f,  4.0f);

    auto v16 = std::make_shared<Vector3>(0.0f,  3.0f,  0.0f);
    auto v17 = std::make_shared<Vector3>(0.0f,  3.0f,  1.0f);
    auto v18 = std::make_shared<Vector3>(0.0f,  3.0f,  2.0f);
    auto v19 = std::make_shared<Vector3>(0.0f,  3.0f,  3.0f);
    auto v20 = std::make_shared<Vector3>(0.0f,  3.0f,  4.0f);

    auto v21 = std::make_shared<Vector3>(0.0f,  4.0f,  0.0f);
    auto v22 = std::make_shared<Vector3>(0.0f,  4.0f,  1.0f);
    auto v23 = std::make_shared<Vector3>(0.0f,  4.0f,  2.0f);
    auto v24 = std::make_shared<Vector3>(0.0f,  4.0f,  3.0f);
    auto v25 = std::make_shared<Vector3>(0.0f,  4.0f,  4.0f);

    auto v26 = std::make_shared<Vector3>(0.0f,  2.0f,  2.2f);

    obj.vertices.push_back(v1);
    obj.vertices.push_back(v2);
    obj.vertices.push_back(v3);
    obj.vertices.push_back(v4);
    obj.vertices.push_back(v5);
    obj.vertices.push_back(v6);
    obj.vertices.push_back(v7);
    obj.vertices.push_back(v8);
    obj.vertices.push_back(v9);
    obj.vertices.push_back(v10);
    obj.vertices.push_back(v11);
    obj.vertices.push_back(v12);
    obj.vertices.push_back(v13);
    obj.vertices.push_back(v14);
    obj.vertices.push_back(v15);
    obj.vertices.push_back(v16);
    obj.vertices.push_back(v17);
    obj.vertices.push_back(v18);
    obj.vertices.push_back(v19);
    obj.vertices.push_back(v20);
    obj.vertices.push_back(v21);
    obj.vertices.push_back(v22);
    obj.vertices.push_back(v23);
    obj.vertices.push_back(v24);
    obj.vertices.push_back(v25);
    obj.vertices.push_back(v26);
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v1));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v2));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v3));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v4));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v5));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v6));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v7));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v8));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v9));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v10));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v11));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v12));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v13));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v14));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v15));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v16));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v17));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v18));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v19));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v20));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v21));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v22));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v23));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v24));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v25));
    obj.mesh0.vertices.push_back(std::make_shared<Vector3>(*v26));


    auto f1 = obj.create_face({"f", "1/1/1",  "2/2/2",  "7/7/7",  "6/6/6"});
    auto f2 = obj.create_face({"f", "2/2/2",  "3/3/3",  "8/8/8",  "7/7/7"});
    auto f3 = obj.create_face({"f", "3/3/3",  "4/4/4",  "9/9/9",  "8/8/8"});
    auto f4 = obj.create_face({"f", "4/4/4",  "5/5/5",  "10/10/10",  "9/9/9"});

    auto f5 = obj.create_face({"f", "6/6/6",  "7/7/7",  "12/12/12",  "11/11/11"});
    auto f6 = obj.create_face({"f", "7/7/7",  "8/8/8",  "13/13/13",  "12/12/12"});
    auto f7 = obj.create_face({"f", "8/8/8",  "13/13/13",  "18/18/18",  "26/26/26"});
    auto f8 = obj.create_face({"f", "8/8/8",  "9/9/9",  "14/14/14",  "26/26/26"});
    auto f9 = obj.create_face({"f", "9/9/9",  "10/10/10",  "15/15/15",  "14/14/14"});

    auto f10 = obj.create_face({"f", "11/11/11",  "12/12/12",  "17/17/17",  "16/16/16"});
    auto f11 = obj.create_face({"f", "12/12/12",  "13/13/13",  "18/18/18",  "17/17/17"});
    auto f12 = obj.create_face({"f", "26/26/26",  "14/14/14",  "19/19/19",  "18/18/18"});
    auto f13 = obj.create_face({"f", "14/14/14",  "15/15/15",  "20/20/20",  "19/19/19"});

    auto f14 = obj.create_face({"f", "16/16/16",  "17/17/17",  "22/22/22",  "21/21/21"});
    auto f15 = obj.create_face({"f", "17/17/17",  "18/18/18",  "23/23/23",  "22/22/22"});
    auto f16 = obj.create_face({"f", "18/18/18",  "19/19/19",  "24/24/24",  "23/23/23"});
    auto f17 = obj.create_face({"f", "19/19/19",  "20/20/20",  "25/25/25",  "24/24/24"});

    obj.faces.push_back(f1);
    obj.faces.push_back(f2);
    obj.faces.push_back(f3);
    obj.faces.push_back(f4);
    obj.faces.push_back(f5);
    obj.faces.push_back(f6);
    obj.faces.push_back(f7);
    obj.faces.push_back(f8);
    obj.faces.push_back(f9);
    obj.faces.push_back(f10);
    obj.faces.push_back(f11);
    obj.faces.push_back(f12);
    obj.faces.push_back(f13);
    obj.faces.push_back(f14);
    obj.faces.push_back(f15);
    obj.faces.push_back(f16);
    obj.faces.push_back(f17);

    obj.link_vertices_to_face();

    obj.add_all_to_heap();

    auto new_v = LocalOperations::diagonal_collapse(obj);

    cr_assert_eq(obj.faces.size(), 16);
    cr_assert_eq(obj.vertices.size(), 25);
    cr_assert_eq(std::find(obj.faces.begin(), obj.faces.end(), f7), obj.faces.end());
    cr_assert_eq(std::find(obj.vertices.begin(), obj.vertices.end(), v13), obj.vertices.end());
    cr_assert_eq(std::find(obj.vertices.begin(), obj.vertices.end(), v26), obj.vertices.end());

    cr_assert_eq(obj.diag_heap.size(), 16);
}
