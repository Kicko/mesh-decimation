//
// Created by hiiro on 4/21/20.
//

# include <criterion/criterion.h>
# include <memory>
# include "face.hh"
# include "vector3.hh"
# include "face_utils.hh"

Test(face_utils, get_normal_face){
    auto a = std::make_shared<Vector3>(0, 0, 0);
    auto b = std::make_shared<Vector3>(1, 0, 0);
    auto c = std::make_shared<Vector3>(1, 1, 0);
    auto d = std::make_shared<Vector3>(0, 1, 0);
    auto f = std::make_shared<Face>(a, b, c, d);
    auto res = std::make_shared<Vector3>(0, 0, 1);
    cr_assert_eq(*res, *get_normal_face(f), "get_normal_face failed");
}

Test(face_utils, get_plan_constant_face) {
    auto a = std::make_shared<Vector3>(0, 0, 0);
    auto b = std::make_shared<Vector3>(1, 0, 0);
    auto c = std::make_shared<Vector3>(1, 1, 0);
    auto d = std::make_shared<Vector3>(0, 1, 0);
    auto f = std::make_shared<Face>(a, b, c, d);
    float res = 0;
    cr_assert_float_eq(res, get_plan_constant_face(f), 0.0000001, "get_plan_constant_face failed");
}

Test(face_utils, dist_face_point) {
    auto a = std::make_shared<Vector3>(0, 0, 0);
    auto b = std::make_shared<Vector3>(1, 0, 0);
    auto c = std::make_shared<Vector3>(1, 1, 0);
    auto d = std::make_shared<Vector3>(0, 1, 0);
    auto f = std::make_shared<Face>(a, b, c, d);
    auto v = Vector3(42, 56, 3.14);
    float res = 3.14;
    cr_assert_eq(res, dist_face_point(f, v), "dist_face_point failed");
}

Test(face_utils, are_connected) {
    auto a = std::make_shared<Vector3>(0, 0, 0);
    auto b = std::make_shared<Vector3>(1, 0, 0);
    auto c = std::make_shared<Vector3>(1, 1, 0);
    auto d = std::make_shared<Vector3>(0, 1, 0);
    auto e = std::make_shared<Vector3>(0, 0, 1);
    auto f = std::make_shared<Vector3>(0, 1, 1);
    auto f1 = std::make_shared<Face>(a, b, c, d);
    auto f2 = std::make_shared<Face>(c, d, e, f);
    cr_assert(are_connected(f1, f2), "are_connected failed");
}

Test(face_utils, connected_to_two_faces) {
    auto a = std::make_shared<Vector3>(0, 0, 0);
    auto b = std::make_shared<Vector3>(1, 0, 0);
    auto c = std::make_shared<Vector3>(1, 1, 0);
    auto d = std::make_shared<Vector3>(0, 1, 0);
    auto e = std::make_shared<Vector3>(0, 0, 1);
    auto f = std::make_shared<Vector3>(0, 1, 1);
    auto f1 = std::make_shared<Face>(a, b, c, d);
    auto f2 = std::make_shared<Face>(a, d, e, f);
    auto g = std::make_shared<Vector3>(1, 1, 1);
    auto t = std::make_shared<Face>(c, d, f, g);
    cr_assert(connected_to_two_faces(t, f1, f2), "connected_to_two_faces failed");
}

Test(face_utils, get_relevant_faces) {
    auto a = std::make_shared<Vector3>(0, 0, 0);
    auto b = std::make_shared<Vector3>(1, 0, 0);
    auto c = std::make_shared<Vector3>(1, 1, 0);
    auto d = std::make_shared<Vector3>(0, 1, 0);
    auto e = std::make_shared<Vector3>(0, 0, 1);
    auto f = std::make_shared<Vector3>(0, 1, 1);
    auto a_m0 = std::make_shared<Vector3>(0, 0, 0);
    auto b_m0 = std::make_shared<Vector3>(1, 0, 0);
    auto c_m0 = std::make_shared<Vector3>(1, 1, 0);
    auto d_m0 = std::make_shared<Vector3>(0, 1, 0);
    auto e_m0 = std::make_shared<Vector3>(0, 0, 1);
    auto f_m0 = std::make_shared<Vector3>(0, 1, 1);
    auto f1 = std::make_shared<Face>(a, b, c, d);
    auto f2 = std::make_shared<Face>(a, d, e, f);
    auto f1_m0 = std::make_shared<Face>(a_m0, b_m0, c_m0, d_m0);
    auto f2_m0 = std::make_shared<Face>(a_m0, d_m0, e_m0, f_m0);
    f1->add_link_mesh0(f1_m0);
    f2->add_link_mesh0(f2_m0);
    a_m0->add_face(f1_m0);
    b_m0->add_face(f1_m0);
    c_m0->add_face(f1_m0);
    d_m0->add_face(f1_m0);
    a_m0->add_face(f2_m0);
    d_m0->add_face(f2_m0);
    e_m0->add_face(f2_m0);
    f_m0->add_face(f2_m0);
    a->add_face(f1);
    b->add_face(f1);
    c->add_face(f1);
    d->add_face(f1);
    a->add_face(f2);
    d->add_face(f2);
    e->add_face(f2);
    f->add_face(f2);
    auto g = std::make_shared<Vector3>(1, 1, 1);
    auto t = std::make_shared<Face>(c, d, f, g);
    auto g_m0 = std::make_shared<Vector3>(1, 1, 1);
    auto t_m0 = std::make_shared<Face>(c_m0, d_m0, f_m0, g_m0);
    t->add_link_mesh0(t_m0);
    c_m0->add_face(t_m0);
    d_m0->add_face(t_m0);
    f_m0->add_face(t_m0);
    g_m0->add_face(t_m0);
    c->add_face(t);
    d->add_face(t);
    f->add_face(t);
    g->add_face(t);
    auto faces = get_relevant_faces(*a);
    auto s = faces.find(f1_m0);
    cr_assert_neq(s, faces.end(), "get_relevant_faces : f1 not added");
    s = faces.find(f2_m0);
    cr_assert_neq(s, faces.end(), "get_relevant_faces : f2 not added");
    s = faces.find(t_m0);
    cr_assert_neq(s, faces.end(), "get_relevant_faces : t not added");
}
