[![coverage](https://gitlab.com/Kicko/mesh-decimation/badges/master/coverage.svg)](https://gitlab.com/Kicko/mesh-decimation/commits/master)

# Mesh decimation

doc: https://cims.nyu.edu/gcl/papers/EGIT10-BozPanPupetall.pdf

# 3D Viewer online (with .obj files)

https://3dviewer.net/
